<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
  $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $projectname=$row ['qm_project_name'];
    $clientid=$row ['qm_clientinfo'];
    $terms=$row ['qm_terms'];
    $usercreated=$row ['qm_usercreated'];
    $month=$row ['qm_imonth'];
    $day=$row ['qm_iday'];
    $year=$row ['qm_iyear'];
    $validity=$row ['qm_validity'];
    $datecreation=$row ['qm_date'];
    $vat=$row ['qm_vat'];
  }
}
$inquirydate="$month-$day-$year";


$queryy = mysql_query( "SELECT * FROM qm_client where qm_client_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $clientid=$row ['qm_client_id'];
    $name=$row ['qm_name'];
    $address=$row ['qm_address'];
    $contact=$row ['qm_contact'];
    $faxno=$row ['qm_faxno'];
    $cperson=$row ['qm_cperson'];

  }
}
		//Item Listing
       $query = "SELECT qm_model, qm_item_name,qm_totalprice,qm_price,qm_desc, count(*) AS counter FROM qm_qtotalprod Where qm_quot_id='$ID' GROUP BY qm_model, qm_item_name";
                $result = mysql_query( $query ) or die( mysql_error() );



                while ( $row = mysql_fetch_array( $result ) ) {

                  $counter=$row['counter'];
                  $model=$row ['qm_model'];
                  $itemname=$row ['qm_item_name'];

                  $price=$row ['qm_price'];
                  $pieces=$row ['qm_pieces'];
                  $desc=$row ['qm_desc'];

                  $queryy = "SELECT qm_model, SUM(qm_pieces) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' and qm_model='$model' GROUP BY qm_model ";
                  $resultt = mysql_query( $queryy ) or die( mysql_error() );
             

  // Print out result
                  while ( $row = mysql_fetch_array( $resultt ) ) {
                    $piecesdb= $row['SUM(qm_pieces)'];
                  }
                  $totalprice = $piecesdb*$price;

                }
//Item Listing
require_once('tcpdf_include.php');

class PDF extends TCPDF {

  public function Header() {
 
  //Fetch for data
   $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $projectname=$row ['qm_project_name'];
    $clientid=$row ['qm_clientinfo'];
    $terms=$row ['qm_terms'];
    $usercreated=$row ['qm_usercreated'];
    $month=$row ['qm_imonth'];
    $day=$row ['qm_iday'];
    $year=$row ['qm_iyear'];
    $validity=$row ['qm_validity'];
    $datecreation=$row ['qm_date'];
     $watermark=$row ['qm_approveby'];
  }
}
$inquirydate="$month-$day-$year";


$queryy = mysql_query( "SELECT * FROM qm_client where qm_client_id ='$clientid'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $clientid=$row ['qm_client_id'];
    $clientname=$row ['qm_name'];
    $clientaddress=$row ['qm_address'];
    $ccontact=$row ['qm_contact'];
    $cfaxno=$row ['qm_faxno'];
    $cperson=$row ['qm_cperson'];

  }
}
  //Fetch for data
 //watermark
  // get the current page break margin
  if($watermark == 2 || $watermark ==3)
  {
      
 }
 else{
   $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = K_PATH_IMAGES.'watermark.jpg';
        $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
 //watermark 
 }
     $this->SetFont('Helvetica','',8);
    // Logo
  
     $image_file = K_PATH_IMAGES.'name.png';
    $this->Image($image_file, 10, 7, 80, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
    
    //Variable for Address
    $address = 'No. 44 Congressional Ave.
Project 8, Quezon City 
Philippines 1100';
    $this->SetXY(10,25);
    $this->MultiCell(60,3,$address,0,'L');

    //Variabe for Contact
    $contact = 'Office Tel. (632)453-57-52 /53
    924-01-48 & 73 / 426-6994
    Fax: (632) 454-53-28
    Email: info@himmax.com
    Website: www.himmax.com';
    $this->SetXY(140,8);
    $this->MultiCell(0,5,$contact,0,R);
    //Client Contact

    $this->SetFont('helvetica','B',10);

    $this->SetXY(10,40);
    $this->MultiCell(90,6,' '.$clientname,1,'L');


    $this->SetFont('Helvetica','',10);

    $this->SetX(10);
    $this->MultiCell(90,18,' '.$clientaddress,1,'L');

    $this->SetFont('Helvetica','',8);

    $this->SetX(10);
    $this->Cell(30,6,' ATTN',1,'L');
    $this->MultiCell(60,6,' '.$cperson,1,'L');
    $this->SetX(10);
    $this->Cell(30,6,' PROJECT',1);
    $this->MultiCell(60,6,' '.$projectname,1,'L');
    $this->SetX(10); 
    $this->Cell(30,6,' TEL/FAX',1);
    $this->MultiCell(60,6,' '.$ccontact." / ".$cfaxno,1,'L');


    //Quotation Details
    $this->SetXY(130,40);
    $this->Cell(30,6,' DATE:',1,'L');
    $this->MultiCell(40,6,' '.$inquirydate,1,'C');
    $this->SetX(130); 
    $this->Cell(30,6,' Quotation No.',1,'L');
    $this->MultiCell(40,6,' '.$ID,1,'C');
    
    $this->SetX(130);
    $this->Cell(30,6,' Date of Inquiry','LR','L');

    $this->MultiCell(40,6,' '.$inquirydate,1,'C');
    // $this->SetX(130);
    // $this->MultiCell(70,6,' '.$inquirydate,'RL','C');

    $this->SetX(130);
    $this->MultiCell(70,6,' TERMS','TLR','L');
    $this->SetX(130);
    $this->MultiCell(70,6,' '.$terms,'RL','C');


    $this->SetX(130);
    $this->MultiCell(70,6,' QUOTATION VALIDITY','TLR','L');
    $this->SetX(130);
    $this->MultiCell(70,6,' '.$validity,'RLB','C');

    $this->SetFont('Helvetica','B',12);
    $this->Ln();
    $this->MultiCell(180,6,'QUOTATION',0,'C');
    $this->SetTextColor(0,0,100);
    $this->SetFont('Helvetica','B',8);
    $this->SetX(5);
    $this->MultiCell(200,6,'FIRE ALARM SYSTEM * DOOR ACCESS SYSTEM * PAGING/BGM SYSTEMS * SYNCHRO-CLOCK SYSTEM * EMERGENCY LIGHT * CCTV',0,'C');
    
	
  }

  // Page footer
  public function Footer() {
  $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
   $usercreate=$row ['qm_usercreated'];
     $watermark=$row ['qm_approveby'];
    $noteno=$row ['qm_note'];
  }
}

$query = mysql_query("SELECT * FROM qm_user where qm_email = '$usercreate'");
$numrows = mysql_num_rows($query);
	if ($numrows !=0)
	{
		while ($row = mysql_fetch_assoc($query))
		{
			$fname=$row ['qm_name'];
			$admin=$row ['qm_level'];
			$title=$row ['qm_title'];
			$dir=$row ['qm_directory'];
		}
	}
	 if($watermark == 2 || $watermark ==3)
  {
         //SIGNATURE OF GX
     $image_file = K_PATH_IMAGES.'GX.png';
     $this->SetX(90);
    $this->Image($image_file, 85, 240, 40, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
     $image_file = K_PATH_IMAGES.$dir;
     $this->SetX(90);
    $this->Image($image_file, 5, 240, 40, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
 }
 else{
 
}
    $this->SetFont('Helvetica','',8);
        $this->SetXY(10,-60);
        $this->Cell(20,20,'
    NOTE:
','LTB','L');


         //Variable for NOTE
if($noteno==1)
{
$note ='***A storage fee will be charge after one (1) month of no response on the above proposal.
***One (1) month warranty for repaired circuit on factory design and materials with free service and replacement of deffective parts 
within the guaranteed period.
***Defects arising from misuse and abnormal conditions are executed from the above warranty.
***Prices are subject to change without prior notice.';
}
else
{
        $note = '(1). One year (1) warranty from the date of delivery on factory design and materials with free services and replacement of defective parts within the guaranteed period. (2). Defects arising from misuse and abnormal conditions are excluded from above warranty. (3). In case check-up / repair of units covered by warranty outside Metro Manila the clients will shoulder the allowances, transportation and accommodation of the service technician. (4). Delivery outside Metro Manila, client will shoulder the freight cost. (5). Prices are subject to change without prior notice.'; 
}
        $this->SetXY(30,-60);
        $this->MultiCell(170,20,$note,'RBT','L');

        $this->SetXY(10,$this->GetY()+5);
        $this->MultiCell(40,-40,'Prepared by:','','L');
        $this->SetXY(10, $this->GetY()+10);
        $this->MultiCell(40,-40,'______________','','L');
        
        $this->SetXY(10,$this->GetY());
        $owner_name='Ismael Reyes';
        $this->MultiCell(40,-40,$fname,'','L');

        $this->SetXY(10,$this->GetY());
        $owner_position='Prepared By';
        $this->MultiCell(40,-40,$title,'','L');

        $this->SetXY(90,-35);
        $this->MultiCell(40,-40,'Approved by:','','L');

        $this->SetXY(90,$this->GetY()+10);
        $this->MultiCell(40,-40,'______________','','L');

        $this->SetXY(90,$this->GetY()+1);
        $approve_name = 'SERAFIN R. GARCIA';
        $this->MultiCell(40,-40,$approve_name,'','L');

        $this->SetXY(90,$this->GetY());
        $approve_position = 'President';
        $this->MultiCell(40,-40,$approve_position,'','L');

        $this->SetXY(145,-35);
        $this->MultiCell(40,-40,'Conforme:');

        $this->SetXY(145,$this->GetY());
        $this->MultiCell(55,-40,'____________________________________________________________________________________________________________________________________________','','L');


    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('helvetica', 'I', 8);
    // Page number
    $this->Cell(330, 10, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
  }
}

// create new PDF document
$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Himmax');
$pdf->SetTitle('Quotation');
$pdf->SetSubject('Quotation');
$pdf->SetKeywords('Himmax, Quotation');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 105, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 60);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

// -----------------------------------------------------------------------------


// -----
      
  $tbl = <<<EOD
<style>
    .qty {
        width: 70px;
    }
    .model {
        width:120px;
    }
    .desc {
        width:260px;
        
    }
    .unit-price {
        width:111px;
    }
    .total-amount {
        width:111px;
    }
</style>

<table border="1" cellpadding="5" width="673px">
    <thead>
        <tr nobr="false">
            <td class="qty">QTY</td>
            <td class="model">MODEL</td>
            <td class="desc">DESCRIPTION</td>
            <td class="unit-price">UNIT PRICE</td>
            <td class="total-amount">TOTAL AMOUNT</td>
        </tr>
    </thead>
    
  
     
EOD;
//itemlsit



                $query = "SELECT qm_model, qm_item_name,qm_totalprice,qm_price,qm_desc, count(*) AS counter FROM qm_qtotalprod Where qm_quot_id='$ID' GROUP BY qm_model, qm_item_name ORDER BY qm_id ASC";
                $result = mysql_query( $query ) or die( mysql_error() );



                while ( $row = mysql_fetch_array( $result ) ) {

                  $counter=$row['counter'];
                  $model=$row ['qm_model'];
                  $itemname=$row ['qm_item_name'];

                  $price=$row ['qm_price'];
                  $pieces=$row ['qm_pieces'];
                  $desc=$row ['qm_desc'];

                  $queryy = "SELECT qm_model, SUM(qm_pieces) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' and qm_model='$model' GROUP BY qm_model  ";
                  $resultt = mysql_query( $queryy ) or die( mysql_error() );

  // Print out result
                  while ( $row = mysql_fetch_array( $resultt ) ) {
                    $piecesdb= $row['SUM(qm_pieces)'];
                  }
                  $totalprice = $piecesdb*$price;
                   $ctotalprice = number_format($totalprice,2);
                   
                     $rprice = number_format($price,2);
                $tbl1 .='
     <tr nobr="true">
        <td class="qty">'.$counter.'</td>
        <td class="model">'.$model.'</td>
        <td class="desc"><span style="font-weight:bold">'.$itemname.'</span></td>
        <td class="unit-price"><span style="text-align:right">'.$rprice.'</span></td>
        <td class="total-amount"><span style="text-align:right">'.$ctotalprice.'</span></td>
     </tr>  
';}

   $queryy = mysql_query( "SELECT * FROM qm_special_desc where qm_quote_id ='$ID' " );
                $numrowss = mysql_num_rows( $queryy );
                if ( $numrowss !=0 ) {
                  while ( $row = mysql_fetch_assoc( $queryy ) ) {
                    $desc=$row ['qm_desc'];
                    $sanitize=nl2br($desc);
    $tbl1 .='
     <tr nobr="true" style="text-align:left">
        <td class="qty"></td>
        <td class="model"></td>
        <td class="desc"><font size ="10">'.$sanitize.'</font></td>
        <td class="unit-price"></td>
        <td class="total-amount"></td>
     </tr>  
';
                   	 }
                    }
                
//itemlist
//special
   $queryy = mysql_query( "SELECT * FROM qm_specialprod where qm_qid ='$ID' " );
                $numrowss = mysql_num_rows( $queryy );
                if ( $numrowss !=0 ) {
                  while ( $row = mysql_fetch_assoc( $queryy ) ) {
                    $specialname=$row ['qm_name'];
                    $specialprices=$row ['qm_price'];
                   	 }
                   	 
                   	 
                    }
                    
                    if(!empty($specialprices))
                   	 {
                   	 $specialprice = number_format($specialprices, 2);
                   	 }
                   	 else
                   	 {
                   	 $specialprice ='';
                   	 }
//special
//discount
     $query = "SELECT * FROM qm_quotation WHERE qm_quotation_id ='$ID' ";

                $result = mysql_query( $query ) or die( mysql_error() );

// Print out result
                while ( $row = mysql_fetch_array( $result ) ) {
                
                
                $first = $row['qm_firstdiscount'];
		$second= $row['qm_seconddiscount'];

                }
//discount
//total price
   $query = "SELECT SUM(qm_price) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' ";

                $result = mysql_query( $query ) or die( mysql_error() );

// Print out result
                while ( $row = mysql_fetch_array( $result ) ) {
                $total=$row['SUM(qm_price)']; 

                }
               
                
                $totalprice = number_format($total,2);
                 if(!$total){
                  $rawtotal="";
                 }
                 else
                 {
               
                    $rawtotal = number_format($total,2); 
                 }
//total price



 if(!empty($first) && empty($second) )
		 {  
		 $percentfirst=".$first";
		 $discount = $total * $percentfirst;
		 $finalfirst= $total-$discount;
		 
		 $totalprice = $specialprices + $finalfirst;
		 if ($vat==2)
		 {
		 $vat ='';
		 
		 }
		 else
		 {
		$vat = $totalprice * .12;
		$wvat ='12% E-VAT';
		$cvat = number_format($vat, 2);
		 }
		 $finalquote = $totalprice + $vat ;
		 //convert
			$cdiscount = number_format($discount,2);
			$cfinalfirst = number_format($finalfirst,2);
			
			$cfinalquote = number_format($finalquote,2);
			
			$ctotalprice = number_format($totalprice,2);
		//convert
		
		
//with 1 discount
$totalammount =' 

</table>
<table border="1" cellpadding="5" width="673px">
  
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
            <td class="desc"><br><br><br><br> <br>'.$specialname.' </td>
            <td class="unit-price"><font size="10"><br> Less '.$first.'% <br><br><br> <br> '.$wvat.'<br> Total </font></td>
            
        <td class="total-amount"><font size="10"><span style="text-align:right"> <br>'.$rawtotal.'  <br> <u>- '.$cdiscount.'</u><br> '.$cfinalfirst.' <br>'.$specialprice.' <br>
         <u>'.$cvat.'</u> <br><b><i> '.$cfinalquote.'</i></b></span></font> 
        </td>
        </tr>
 
</table>
';
//with 1 discount
}
elseif(!empty($second))
		 {
		 $percentfirst=".$first";
		 $discount = $total * $percentfirst;
		 $finalfirst= $total-$discount;
		 
		 $percentsecond=".$second";
		 $twodiscount = $finalfirst * $percentsecond;
		 $finalsecond= $finalfirst-$twodiscount;
		 $totalprice = $specialprices + $finalsecond;
		 if ($vat==2)
		 {
		 
		 $vat ='';
		 }
		 else
		 {
		$vat = $totalprice * .12;
		$wvat ='12% E-VAT';
		$cvat = number_format($vat, 2);
		 }
		 $finalquote = $totalprice + $vat;
		 //convert
			$cdiscount = number_format($discount, 2);
			$cfinalfirst = number_format($finalfirst, 2);
			$ctwodiscount = number_format($twodiscount, 2);
			$cfinalsecond = number_format($finalsecond, 2);
			
			$cfinalquote = number_format($finalquote, 2);
			
		//convert
//with 2 discount
$totalammount =' </table>
<table border="1" cellpadding="5" width="675px">
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
       		 
            <td class="desc"> <br><br><br><br> <br> <br>  <br> <br>'.$specialname.'  </td>
               <td class="unit-price"><font size="10"> <br>Less '.$first.'% 
               <br> <br> <br> <br>  Special Less '.$second.'%
                <br> <br> <br>  '.$wvat.' 
                   <br> <br>  Total</font>
               </td>
               
        <td class="total-amount"><font size="10"><span style="text-align:right"> <br>'.$rawtotal.'<br><u>-'.$cdiscount.'</u><br>'.$cfinalfirst.'
        <br> <br> <u>- '.$ctwodiscount.'</u><br>'.$cfinalsecond.' <br>'.$specialprice.' <br>
            <u> '.$cvat.'</u><br> 
          <br> <i> <b>'.$cfinalquote.'</b></span></font> </i>
        </td>
        </tr>
 
</table>
';
//with 2 discount
}

else{
//without

 		 $totalprice = $specialprices + $total;
		 if ($vat==2)
		 {
		 
		 $vat ='';
		 }
		 else
		 {
		$vat = $totalprice * .12;
		$wvat ='12% E-VAT';
		$cvat = number_format($vat, 2);
		 }
		 $finaltotal = $totalprice + $vat;
		 //convert
			
			$cfinaltotal = number_format($finaltotal,2);
			
		//convert

$totalammount =' </table>
<table border="1" cellpadding="5" width="675px">
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
            <td class="desc"><br><br> <br>'.$specialname.'  </td>
               <td class="unit-price"><font size="10"><span style="text-align:right">
            <br>  Amount <br><br> <br> '.$wvat.' <br> <br>TOTAL</font>
               </span></td>
        <td class="total-amount"><font size="10"><span style="text-align:right"><br>'.$rawtotal.' <br>'.$specialprice.' <br>
        <br> <u> '.$cvat.'</u> <br><br><i> <b>'.$cfinaltotal.'</b></span></i></font>
        </td>
        </tr>
 
</table>
</body>
';
//without
}
$pdf->writeHTML($tbl . $tbl1 . $tbl2. $totalammount, false, false, false, false, 'C');
// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+