<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

class PDF extends TCPDF {

  public function Header() {
     $this->SetFont('Helvetica','',8);
    // Logo
    $image_file = K_PATH_IMAGES.'name.png';
    $this->Image($image_file, 10, 7, 80, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
    //Variable for Address
    $address = 'No. 44 Congressional Ave.
Project 8, Quezon City 
Philippines 1100';
    $this->SetXY(10,25);
    $this->MultiCell(60,3,$address,0,'L');

    //Variabe for Contact
    $contact = 'Office Tel. (532)453-57-52 /53
    924-01-48 & 73 / 426-6994
    Fax: (632) 454-53-28
    Email: info@himmax.com
    Website: www.himmax.com';
    $this->SetXY(140,8);
    $this->MultiCell(0,5,$contact,0,R);
    //Client Contact
    $this->SetXY(10,40);
    $this->MultiCell(90,6,' '.'$name',1,'L');
    $this->SetX(10);
    $this->MultiCell(90,6,' '.'$Address',1,'L');
    $this->SetX(10);
    $this->Cell(30,6,' ATTN',1,'L');
    $this->MultiCell(60,6,' '.'Mr.$name',1,'L');
    $this->SetX(10);
    $this->Cell(30,6,' PROJECT',1);
    $this->MultiCell(60,6,' '.'Mr. $project_name',1,'L');
    $this->SetX(10); 
    $this->Cell(30,6,' TEL/FAX',1);
    $this->MultiCell(60,6,' '.'xx-xxx',1,'L');


    //Quotation Details
    $this->SetXY(130,40);
    $this->Cell(30,6,' DATE:',1,'L');
    $this->MultiCell(40,6,' '.'11/6/2013',1,'C');
    $this->SetX(130); 
    $this->Cell(30,6,' Quotation No.',1,'L');
    $this->MultiCell(40,6,' '.'13-11-4738',1,'C');
    
    $this->SetX(130);
    $this->MultiCell(70,6,' Date of Inquiry','LR','L');
    $this->SetX(130);
    $this->MultiCell(70,6,' '.'$September 6, 0213','RL','C');

    $this->SetX(130);
    $this->MultiCell(70,6,' TERMS','TLR','L');
    $this->SetX(130);
    $this->MultiCell(70,6,' '.'$50% DOwn et gijewj ','RL','C');


    $this->SetX(130);
    $this->MultiCell(70,6,' QUOTATION VALIDITY','TLR','L');
    $this->SetX(130);
    $this->MultiCell(70,6,' '.'$1 Month ','RLB','C');


    $this->SetTextColor(0,0,100);
    $this->SetFont('Helvetica','B',8);
    $this->Ln();
    $this->MultiCell(180,6,'FIRE ALARM SYSTEM * DOOR ACCESS SYSTEM * PAGING/BGM SYSTEMS * SYNCHRO-CLOCK SYSTEM * EMERGENCY LIGHT',0,'C');

  }

  // Page footer
  public function Footer() {

    $this->SetFont('Helvetica','',8);
        $this->SetXY(10,-60);
        $this->Cell(20,20,'
    NOTE:
','LTB','L');


        $this->SetXY(30,-60);
        $this->MultiCell(170,20,'(1). One year (1) warranty from the date of delivery on factory design and materials with free services and replacement of defective parts within the guaranteed period. (2). Defects arising from misuse and abnormal conditions are excluded from above warranty. (3). In case check-up / repair of units covered by warranty outside Metro Manila the clients will shoulder the allowances, transportation and accommodation of the service technician. (4). Delivery outside Metro Manila, client will shoulder the freight cost. (5). Prices are subject to change without prior notice.','RBT','L');

        $this->SetXY(10,$this->GetY()+5);
        $this->MultiCell(40,-40,'Very truly yours,','','L');
        $this->SetXY(10, $this->GetY()+10);
        $this->MultiCell(40,-40,'______________','','L');
        
        $this->SetXY(10,$this->GetY());
        $owner_name='Ismael Reyes';
        $this->MultiCell(40,-40,$owner_name,'','L');

        $this->SetXY(10,$this->GetY());
        $owner_position='Account Manager';
        $this->MultiCell(40,-40,$owner_position,'','L');

        $this->SetXY(90,-35);
        $this->MultiCell(40,-40,'Approved by:','','L');

        $this->SetXY(90,$this->GetY()+10);
        $this->MultiCell(40,-40,'______________','','L');

        $this->SetXY(90,$this->GetY()+1);
        $approve_name = 'SERAFIN R. GARCIA';
        $this->MultiCell(40,-40,$approve_name,'','L');

        $this->SetXY(90,$this->GetY());
        $approve_position = 'VP Marketing';
        $this->MultiCell(40,-40,$approve_position,'','L');

        $this->SetXY(145,-35);
        $this->MultiCell(40,-40,'Conforme:');

        $this->SetXY(145,$this->GetY());
        $this->MultiCell(55,-40,'____________________________________________________________________________________________________________________________________________','','L');


    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('helvetica', 'I', 8);
    // Page number
    $this->Cell(330, 10, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
  }
}

// create new PDF document
$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Himmax');
$pdf->SetTitle('Quotation');
$pdf->SetSubject('Quotation');
$pdf->SetKeywords('Himmax, Quotation');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 105, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 60);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 10);

// add a page
$pdf->AddPage();

// -----------------------------------------------------------------------------
  $content = '<tr nobr="true">
        <td class="qty">4</td>
        <td class="model">AR-721HB</td>
        <td class="desc">STAND-ALONE/NETWORKING PROXIMITY CONTROLLER</td>
        <td class="unit-price">4,800.00</td>
        <td class="total-amount">19,200.00</td>
    </tr>
    <tr nobr="true">
        <td class="qty">4</td>
        <td class="model">AR-721HB</td>
        <td class="desc">STAND-ALONE/NETWORKING PROXIMITY CONTROLLER</td>
        <td class="unit-price">4,800.00</td>
        <td class="total-amount">19,200.00</td>
    </tr>
    <tr nobr="true">
        <td class="qty">4</td>
        <td class="model">AR-721HB</td>
        <td class="desc">STAND-ALONE/NETWORKING PROXIMITY CONTROLLER</td>
        <td class="unit-price">4,800.00</td>
        <td class="total-amount">19,200.00</td>
    </tr>
    <tr nobr="true">
        <td class="qty">4</td>
        <td class="model">AR-721HB</td>
        <td class="desc">STAND-ALONE/NETWORKING PROXIMITY CONTROLLER</td>
        <td class="unit-price">4,800.00</td>
        <td class="total-amount">19,200.00</td>
    </tr>';
  $tbl = <<<EOD
<style>
    .qty {
        width: 70px;
    }
    .model {
        width:120px;
    }
    .desc {
        width:260px;
        
    }
    .unit-price {
        width:111px;
    }
    .total-amount {
        width:111px;
    }
</style>
<table border="1" cellpadding="5" width="673px">
    <thead>
        <tr nobr="false">
            <td class="qty">QTY</td>
            <td class="model">MODEL</td>
            <td class="desc">DESCRIPTION</td>
            <td class="unit-price">UNIT PRICE</td>
            <td class="total-amount">TOTAL AMOUNT</td>
        </tr>
    </thead>
    $content
</table>
EOD;
$pdf->writeHTML($tbl, false, false, false, false, 'C');
// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+