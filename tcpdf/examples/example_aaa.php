<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
 $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $projectname=$row ['qm_project_name'];
    $clientid=$row ['qm_clientinfo'];
    $terms=$row ['qm_terms'];
    $usercreated=$row ['qm_usercreated'];
    $month=$row ['qm_imonth'];
    $day=$row ['qm_iday'];
    $year=$row ['qm_iyear'];
    $validity=$row ['qm_validity'];
    $datecreation=$row ['qm_date'];
    $vat=$row ['qm_vat'];
       $pono=$row ['qm_pono'];
  }
}
$inquirydate="$month-$day-$year";


$queryy = mysql_query( "SELECT * FROM qm_client where qm_client_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $clientid=$row ['qm_client_id'];
    $name=$row ['qm_name'];
    $address=$row ['qm_address'];
    $contact=$row ['qm_contact'];
    $faxno=$row ['qm_faxno'];
    $cperson=$row ['qm_cperson'];

  }
}
		//Item Listing
       $query = "SELECT qm_model, qm_item_name,qm_totalprice,qm_price,qm_desc, count(*) AS counter FROM qm_qtotalprod Where qm_quot_id='$ID' GROUP BY qm_model, qm_item_name";
                $result = mysql_query( $query ) or die( mysql_error() );



                while ( $row = mysql_fetch_array( $result ) ) {

                  $counter=$row['counter'];
                  $model=$row ['qm_model'];
                  $itemname=$row ['qm_item_name'];

                  $price=$row ['qm_price'];
                  $pieces=$row ['qm_pieces'];
                  $desc=$row ['qm_desc'];

                  $queryy = "SELECT qm_model, SUM(qm_pieces) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' and qm_model='$model' GROUP BY qm_model ";
                  $resultt = mysql_query( $queryy ) or die( mysql_error() );
             

  // Print out result
                  while ( $row = mysql_fetch_array( $resultt ) ) {
                    $piecesdb= $row['SUM(qm_pieces)'];
                  }
                  $totalprice = $piecesdb*$price;

                }
//Item Listing
require_once('tcpdf_include.php');

class PDF extends TCPDF {

  public function Header() {
  
  //Fetch for data
   $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $projectname=$row ['qm_project_name'];
    $clientid=$row ['qm_clientinfo'];
    $terms=$row ['qm_terms'];
    $usercreated=$row ['qm_usercreated'];
    $month=$row ['qm_imonth'];
    $day=$row ['qm_iday'];
    $year=$row ['qm_iyear'];
    $validity=$row ['qm_validity'];
    $datecreation=$row ['qm_date'];
     $vat=$row ['qm_vat'];
    $pono=$row ['qm_pono'];
  }
}
$inquirydate="$month-$day-$year";

$queryy = mysql_query( "SELECT * FROM qm_sales_order where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $salesid=$row ['qm_sales_id'];
    $salesdate=$row ['qm_date'];
    }
}
$queryy = mysql_query( "SELECT * FROM qm_client where qm_client_id ='$clientid'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $clientid=$row ['qm_client_id'];
    $clientname=$row ['qm_name'];
    $clientaddress=$row ['qm_address'];
    $ccontact=$row ['qm_contact'];
    $cfaxno=$row ['qm_faxno'];
    $cperson=$row ['qm_cperson'];

  }
}
  //Fetch for data
  
    $this->SetFont('times','',20);
    // header
    $this->SetX(60);
    $this->MultiCell(100,6,'Himmax Electronics Corporation','C');
    
    $this->SetXY(90,$this->GetY()+1);
    $this->SetFont('times','',15);
    $this->MultiCell(25,6,'Sales Order','C');
    
    $this->SetFont('Helvetica','',10);
    $this->SetXY(160, $this->GetY()+5);
    //SO NUMBER VARIABLE
    $so_number = '14-1911';
    $this->MultiCell(40,6,'S.O. No. :    '.$salesid,0,'L');

    //DATE
    $date_variable = 'Apr-08-2014';
    $this->SetXY(160, $this->GetY());
    $this->MultiCell(50,6,'Date:             '.$salesdate,0,'L');

    $this->SetX(10);
    //Customer Var
    $customer_name = '4Bs TelCommunication & Electrical Enterprises';
    $this->MultiCell(150,6,'P.O.    '.$pono,0,'L');
    
    $this->SetX(10);
    //Customer Var
    $customer_name = '4Bs TelCommunication & Electrical Enterprises';
    $this->MultiCell(130,6,'Customer:    '.$clientname,0,'L');

    $this->SetXY(10,$this->GetY());
    $this->Cell(20,6,'Address: ',0,'L');
    $this->SetXY(30,$this->GetY()+1);
    //Custom Address VAR
    $customer_address = 'Blk 11 Lot 1 Graceland Subd. Ampid II San Mateo Rizal';
    $this->MultiCell(90,6,$clientaddress,0,'L');

    $this->SetXY(10,$this->GetY());
    $this->Cell(20,6,'Deliver to:  ',0,'L');

    $this->SetXY(30,$this->GetY()+1);
    //deliver to address VAR
    $deliver_to = '';
    $this->MultiCell(90,6,$deliver_to,0,'L');

    //customer tel VAR
    $customer_tel = '999-00-11';

    $this->SetXY(10,$this->GetY());
    $this->Cell(20,6,'Tel.No.: ',0,'L');

    $this->SetXY(30,$this->GetY()+1);
    $this->MultiCell(90,6,$ccontact,0,'L');

    //ATTN VAR
    $customer_attn = 'Mr Ben Abulencia';

    $this->SetXY(10,$this->GetY());
    $this->Cell(20,6,'Attn: ',0,'L');

    $this->SetXY(30,$this->GetY()+1);
    $this->MultiCell(90,6,$cperson,0,'L');

  }

  // Page footer
  public function Footer() {
  $ID = ( $_GET['ID'] );
 
  $connect = mysql_connect ("localhost","dmdevtea_admin","@cc0unt12345") or die ("no connection");
mysql_select_db("dmdevtea_qmaker") or die ("no DB");

$queryy = mysql_query( "SELECT * FROM qm_quotation where qm_quotation_id ='$ID'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $projectname=$row ['qm_project_name'];
    $clientid=$row ['qm_clientinfo'];
    $terms=$row ['qm_terms'];
    $usercreated=$row ['qm_usercreated'];
    $month=$row ['qm_imonth'];
    $day=$row ['qm_iday'];
    $year=$row ['qm_iyear'];
    $validity=$row ['qm_validity'];
    $datecreation=$row ['qm_date'];
  }
}

$queryy = mysql_query( "SELECT * FROM qm_user where qm_email ='$usercreated'" );
$numrowss = mysql_num_rows( $queryy );
if ( $numrowss !=0 ) {
  while ( $row = mysql_fetch_assoc( $queryy ) ) {
    $name=$row ['qm_name'];
 	$title=$row ['qm_title'];
  }
}
    $this->SetFont('Helvetica','',10);


    $this->SetY(-60);
    $this->Cell(10,6,'Terms: ',0,'L');

    //VAR for TERMS
    $var_terms='COD';
    $this->SetXY(50,$this->GetY()+1);
    $this->MultiCell(90,6,$terms,0,'L');

    $this->SetXY(10,$this->GetY());
    $this->MultiCell(90,6,'Balance of Account: ',0,'L');

    $this->SetXY(10,$this->GetY());
    $this->MultiCell(90,6,'Age of Account: ',0,'L');

    $this->SetXY(10,$this->GetY());
    $this->MultiCell(90,6,'Remarks: ',0,'L');

    $this->SetXY(10,-25);
    $this->Cell(30,6,'Prepared by: ',0,'L');
    
    //var for recommended by
    $recommend_by = 'MAIDA P. SARMIENTO';

    $this->SetX(45);
    $this->Cell(40,6,$name,0,'L');

    //var for recommended by POSITION
    $recommend_position = 'BD Manager';
    $this->SetXY(45,$this->GetY()+5);
    $this->MultiCell(40,6,$title,0,'L');

    $this->SetXY(140,-25);
    $this->Cell(30,6,'Approved by: ',0,'L');

    $this->SetX(175);
    $approve_by = 'Serafin Garcia';
    $this->MultiCell(90,6,$approve_by,0,'L');
    $this->SetXY(175,$this->GetY());
    $approve_position ='President';
    $this->MultiCell(90,6,$approve_position,0,'L');

    // Position at 15 mm from bottom
    $this->SetY(-15);
    // Set font
    $this->SetFont('helvetica', 'I', 8);
    // Page number
    $this->Cell(330, 10, 'Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
  }
}

// create new PDF document
$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Himmax');
$pdf->SetTitle('Quotation');
$pdf->SetSubject('Quotation');
$pdf->SetKeywords('Himmax, Quotation');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 80, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 60);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 10);

// add a page
$pdf->AddPage();

// -----------------------------------------------------------------------------
  $tbl = <<<EOD
<style>
    .po {
        width:100px;
    }
    .item {
        width:50px;
    }
    .desc {
        width:250px;
    }
    .unit {
        width:50px;
    }
    .qty {
        width:70px;
    }
    .unit-p {
        width:90px;
    }
    .amount {
        width:90px;
    }
</style>
<table border="1" cellpadding="5" width="673px">
    <thead>
        <tr nobr="false">
            <td class="qty">QTY</td>
            <td class="model">MODEL</td>
            <td class="desc">DESCRIPTION</td>
            <td class="unit-price">UNIT PRICE</td>
            <td class="total-amount">TOTAL AMOUNT</td>
        </tr>
    </thead>

EOD;
//itemlsit



                $query = "SELECT qm_model, qm_item_name,qm_totalprice,qm_price,qm_desc, count(*) AS counter FROM qm_qtotalprod Where qm_quot_id='$ID' GROUP BY qm_model, qm_item_name";
                $result = mysql_query( $query ) or die( mysql_error() );



                while ( $row = mysql_fetch_array( $result ) ) {

                  $counter=$row['counter'];
                  $model=$row ['qm_model'];
                  $itemname=$row ['qm_item_name'];

                  $price=$row ['qm_price'];
                  $pieces=$row ['qm_pieces'];
                  $desc=$row ['qm_desc'];

                  $queryy = "SELECT qm_model, SUM(qm_pieces) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' and qm_model='$model' GROUP BY qm_model ";
                  $resultt = mysql_query( $queryy ) or die( mysql_error() );

  // Print out result
                  while ( $row = mysql_fetch_array( $resultt ) ) {
                    $piecesdb= $row['SUM(qm_pieces)'];
                  }
                  $totalprice = $piecesdb*$price;
                   $ctotalprice = number_format($totalprice,2);
                $tbl1 .='
     <tr nobr="true">
        <td class="qty">'.$counter.'</td>
        <td class="model">'.$itemname.'</td>
        <td class="desc"><span style="font-weight:bold">'.$model.'</span></td>
        <td class="unit-price"><span style="text-align:right">'.$price.'</span></td>
        <td class="total-amount"><span style="text-align:right">'.$ctotalprice.'</span></td>
     </tr>  
';}

   $queryy = mysql_query( "SELECT * FROM qm_special_desc where qm_quote_id ='$ID' " );
                $numrowss = mysql_num_rows( $queryy );
                if ( $numrowss !=0 ) {
                  while ( $row = mysql_fetch_assoc( $queryy ) ) {
                    $desc=$row ['qm_desc'];
                    $sanitize=nl2br($desc);
    $tbl1 .='
     <tr nobr="true" style="text-align:left">
        <td class="qty"></td>
        <td class="model"></td>
        <td class="desc"><font size ="10">'.$sanitize.'</font></td>
        <td class="unit-price"></td>
        <td class="total-amount"></td>
     </tr>  
';
                   	 }
                    }
                
//itemlist
//special
   $queryy = mysql_query( "SELECT * FROM qm_specialprod where qm_qid ='$ID' " );
                $numrowss = mysql_num_rows( $queryy );
                if ( $numrowss !=0 ) {
                  while ( $row = mysql_fetch_assoc( $queryy ) ) {
                    $specialname=$row ['qm_name'];
                    $specialprice=$row ['qm_price'];
                   	 }
                    }
//special
//discount
     $query = "SELECT * FROM qm_quotation WHERE qm_quotation_id ='$ID' ";

                $result = mysql_query( $query ) or die( mysql_error() );

// Print out result
                while ( $row = mysql_fetch_array( $result ) ) {
                
                
               $first = $row['qm_firstdiscount'];
         
                  
                  
		$second= $row['qm_seconddiscount'];
	
		
                }
//discount
//total price
   $query = "SELECT SUM(qm_price) FROM qm_qtotalprod WHERE qm_quot_id ='$ID' ";

                $result = mysql_query( $query ) or die( mysql_error() );

// Print out result
                while ( $row = mysql_fetch_array( $result ) ) {
                $total=$row['SUM(qm_price)']; 

                }
                $totalprice = number_format($total,2);
                 $rawtotal = number_format($total,2);
//total price



 if(!empty($first) && empty($second) )
		 {  
		 $percentfirst=".$first";
		 $discount = $total * $percentfirst;
		 $finalfirst= $total-$discount;
		 
		 $totalprice = $specialprice + $finalfirst;
		 
		 if ($vat==2)
		 {
		 
		 $vat ='0';
		 }
		 else
		 {
		$vat = $totalprice * .12;
		 }
		 $finalquote = $totalprice + $vat ;
		 //convert
			$cdiscount = number_format($discount,2);
			$cfinalfirst = number_format($finalfirst,2);
			$cvat = number_format($vat,2);
			$cfinalquote = number_format($finalquote,2);
			$cspecialprice = number_format($specialprice,2);
			$ctotalprice = number_format($totalprice,2);
		//convert
		
		
//with 1 discount
$totalammount =' 

</table>
<table border="1" cellpadding="5" width="673px">
  
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
            <td class="desc"><br><br><br><br> <br>'.$specialname.' </td>
            <td class="unit-price"><font size="10"><br> Less '.$first.'% <br><br><br> <br> 12% E-VAT <br> Total </font></td>
            
        <td class="total-amount"><font size="10"><span style="text-align:right"> <br>'.$rawtotal.'  <br> <u>- '.$cdiscount.'</u><br> '.$cfinalfirst.' <br>'.$specialprice.' <br>
         <u> '.$cvat.'</u> <br><b><i> '.$cfinalquote.'</i></b></span></font> 
        </td>
        </tr>
 
</table>
';
//with 1 discount
}
elseif(!empty($second))
		 {
		 $percentfirst=".$first";
		 $discount = $total * $percentfirst;
		 $finalfirst= $total-$discount;
		 
		 $percentsecond=".$second";
		 $twodiscount = $finalfirst * $percentsecond;
		 $finalsecond= $finalfirst-$twodiscount;
		 $totalprice = $specialprice + $finalsecond;
		  if ($vat==2)
		 {
		 
		 $vat ='0';
		 }
		 else
		 {
		$vat = $totalprice * .12;
		 }
		 $finalquote = $totalprice + $vat;
		 //convert
			$cdiscount = number_format($discount, 2);
			$cfinalfirst = number_format($finalfirst, 2);
			$ctwodiscount = number_format($twodiscount, 2);
			$cfinalsecond = number_format($finalsecond, 2);
			$cvat = number_format($vat, 2);
			$cspecialprice = number_format($specialprice,2);
			$cfinalquote = number_format($finalquote, 2);
			
		//convert
//with 2 discount
$totalammount =' </table>
<table border="1" cellpadding="5" width="675px">
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
       		 
            <td class="desc"> <br><br><br><br> <br> <br>  <br> <br>'.$specialname.'  </td>
               <td class="unit-price"><font size="10"> <br>Less '.$first.'% 
               <br> <br> <br> <br>  Special Less '.$second.'%
                <br><br> <br> <br>  12% E-VAT 
                   <br> <br>  Total</font>
               </td>
               
        <td class="total-amount"><font size="10"><span style="text-align:right"> <br>'.$rawtotal.'<br><u>-'.$cdiscount.'</u><br>'.$cfinalfirst.'
        <br> <br> <u>- '.$ctwodiscount.'</u><br>'.$cfinalsecond.' <br>'.$specialprice.' <br>
           <br> <u> '.$cvat.'</u><br> 
          <br> <i> <b>'.$cfinalquote.'</b></span></font> </i>
        </td>
        </tr>
 
</table>
';
//with 2 discount
}

else{
//without

 		 $totalprice = $specialprice + $total;
		 if ($vat==2)
		 {
		 
		 $vat ='0';
		 }
		 else
		 {
		$vat = $totalprice * .12;
		 }
		 $finaltotal = $totalprice + $vat;
		 //convert
			
			$cvat = number_format($vat, 2);
			$cfinaltotal = number_format($finaltotal,2);
			$cspecialprice = number_format($specialprice,2);
		//convert

$totalammount =' </table>
<table border="1" cellpadding="5" width="675px">
        <tr nobr="true">
          <td class="qty"></td>
       		 <td class="model"></td>
            <td class="desc"><br><br> <br>'.$specialname.'  </td>
               <td class="unit-price"><font size="10"><span style="text-align:right">
             <br>  Ammount <br><br> <br> 12% E-VAT <br> <br>TOTAL</font>
               </span></td>
        <td class="total-amount"><font size="10"><span style="text-align:right"><br>'.$rawtotal.' <br>'.$specialprice.' <br>
        <br> <u> '.$cvat.'</u> <br><br><i> <b>'.$cfinaltotal.'</b></span></i></font>
        </td>
        </tr>
 
</table>
';
//without
}
$pdf->writeHTML($tbl . $tbl1 . $tbl2. $totalammount, false, false, false, false, 'C');
//$pdf->writeHTML($tbl, false, false, false, false, 'C');
// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_048.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+