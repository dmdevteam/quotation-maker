<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Sales Order - Himmax</title>
    <?php include '../header/header-inc.php'; ?>
   
  </head>

  <body>

    <div id="wrapper">

      <?php include '../header/header-admin.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12 hidden-print">
            <h1>Sales Order</h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Dashboard</a></li>
              <li><a href="index.html"><i class="icon-dashboard"></i> Forms</a></li>
              <li class="active"><i class="icon-file-alt"></i> Sales Order</li>
            </ol>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <h3 class="text-center">Himmax Electronics Corporation <br><small>Sales Order</small></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6">
                <table class="table well">
                  <tr>
                    <td>Customer</td>
                    <td>SAMPLE CUSTOMER ENGINEERING</td>
                  </tr>
                  <tr>
                    <td>Address:</td>
                    <td>Dumaguete City</td>
                  </tr>
                  <tr>
                    <td>Deliver to:</td>
                    <td>Contact # 225-xx-xx / 0905-xxxxxx</td>
                  </tr>
                  <tr>
                    <td>Attn:</td>
                    <td>Ms. Lowela</td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-offset-3 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-6">
                <table class="table well">
                  <tr>
                    <td>
                      S.O. NO:
                    </td>
                    <td>
                      14-000X
                    </td>
                  </tr>
                  <tr>
                    <td>Date :
                    </td>
                    <td>Feb 14, 2014</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-bordered">
                  <tr>
                    <th>P.O. No.</th>
                    <th>Item</th>
                    <th>DESCRIPTION</th>
                    <th>Unit</th>
                    <th>Qty</th>
                    <th>Unit P.</th>
                    <th>Amount</th>
                  </tr>
                  <tr>
                    <td>PO#CONFORME</td>
                    <td>1)</td>
                    <td>LABOR & METRIALS COST FOR REPAIR OF FIRE ALARM CONTROL PANEL 8ZONES (MOTHERBOARD) <br>SCOPE OF WORKS:<br>*Checking & Testing of units <br>*Replacement of defective parts<br>*Testing of Units<br>Note:<br>*One(1) month warranty etc. etc. etc.</td>
                    <td></td>
                    <td>1</td>
                    <td>5000</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Amount P</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total P</td>
                    <td>5000</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <small>
                  <table class="table well">
                    <tr>
                      <td>Terms</td>
                      <td> B 2 B</td>
                    </tr>
                    <tr>
                      <td>Credit Limit:</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Balance of Account</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Age of Account</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Remark</td>
                      <td></td>
                    </tr>
                  </table>
                  
                </small>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                Recommended by: 
                <hr><p class="text-center">BDC</p>
              </div>
              <div class="col-lg-offset-2 col-lg-5 col-xs-offset-2 col-xs-5 col-md-offset-2 col-md-5 col-sm-offset-2 col-sm-2">
                Approved by:
                <hr><p class="text-center">President</p>
              </div>
            </div>
          </div><!--/.container -->
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

  </body>
</html>