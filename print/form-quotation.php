<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Quotation - Himmax</title>
    <?php include '../header/header-inc.php';?>  
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php include '../header/header-admin.php'; ?>

      <div id="page-wrapper">
        <div class="row">
          <!-- header/breadcrumb -->
          <div class="col-lg-12 hidden-print">
            <h1>Quotation Form</h1>
            <ol class="breadcrumb">
              <li><a href="index.html"><i class="icon-dashboard"></i> Dashboard</a></li>
              <li><a href="forms.html"><i class="icon-file-alt"></i> Forms</a></li>
              <li class="active"><i class="icon-file-alt"> Quotation</i></li>
            </ol>
          </div><!-- /.col-lg-12 header / breadcrumb-->
          <!-- content -->
          <div class="container"> 
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                <img src="../img/himmax-logo.png" alt="">
                <address>
                  201 Second Floor Congressional Plaza Bldg.,<br>
                  51 Congressional Ave., Project 8,<br>
                  Quezon City, Philippines 1100<br>
                </address>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 pull-right">
                <address class="text-right">
                  Office Tel.: (632) 453-57-52 / 53<br>
                   924-01-48 & 73 / 426-6994<br>
                  Fax:(632)454-53-28<br>
                  Email: info@himmax.com<br>
                  Website: www.himmax.com
                </address>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <table class="table table-bordered">
                  <tr>
                    <td colspan="2">
                      NAME / COMPANY ADDRESS
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      DIVERSITY EECT<br><br><br><br>
                    </td>
                  </tr>
                  <tr>
                    <th>Attn:</th>
                    <td>MS.FLORENCE</td>
                  </tr>
                  <tr>
                    <td>
                      PROJECT:
                    </td>
                    <td>
                      SUPPLY OF DOOR ACCESS
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      TEL /FAX
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-offset-4 col-xs-4">
                <table class="table table-bordered">
                  <tr>
                    <td>DATE:</td>
                    <td>11/28/2013</td>
                  </tr>
                  <tr>
                    <td>Quotation No.</td>
                    <td>13-11-4883</td>
                  </tr>
                  <tr>
                    <td>DATE OF INQUIRY</td>
                    <td>NOV 27, 2013</td>
                  </tr>
                  <tr>
                    <td>TERMS</td>
                    <td>CASH UPON PICK-UP</td>
                  </tr>
                  <tr>
                    <td>
                      QUOTATION VALIDATION
                    </td>
                    <td>1 MONTH</td>
                  </tr>
                </table>
              </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <small>
                  FIRE ALARM SYSTEMS * INTRUDER ALARM SYSTEMS * PAGING/BGM SYSTEMS * SYNCHRO-CLOCK SYSTEM * AUTO EMERGENCY LIGHT
                </small>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <h3><u>QUOTATION</u></h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <small>
                  We are pleased to submit our quotation hereunder:
                </small>
                <table class="table table-bordered">
                  <tr>
                    <th>QTY</th>
                    <th>MODEL</th>
                    <th>DESCRIPTION</th>
                    <th>UNIT PRICE</th>
                    <th>Total Amount</th>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>AR-821EFB5</td>
                    <td>V5 LCD BIOMETRIC ACCESS CONTROLLER</td>
                    <td>5000</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>BP-W15-2400-U</td>
                    <td>SWITCHING AC ADAPTOR</td>
                    <td>5000</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>AR-0300M</td>
                    <td>ELECTROMAGNETIC LOCK 300Lbs. 12VDC/24VDC</td>
                    <td>5000</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>AR-TAGCI1</td>
                    <td>125kHz, ISO Card (Read and Write) 
                      <br>
                        NOTE:
                        
                        <br>* Supply of units only. Delivery outside Metro Manila Client will shoulder the cost
                        <br>* This quotation is based on Clients requirements thru phone calls, and it will change upon actual site survey. 
                    </td>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Less 15%</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Amount P</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>12% E-VAT P</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>TOTAL P</td>
                    <td>5000</td>
                  </tr>
                  <tr>
                    <td colspan="5">NOTE : orem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu neque non dui commodo tempor id ut magna. Mauris vel dignissim risus, tempor semper felis. Etiam a turpis quis neque tincidunt eleifend. Fusce a malesuada orci, at volutpat sem. Suspendisse aliquam metus felis, scelerisque placerat nibh adipiscing vitae. Phasellus sit amet rhoncus diam. Sed ut porta sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque hendrerit, justo eu luctus sagittis, nulla nisl sagittis lorem, non consequat quam magna ac enim. Sed a dignissim mi. Vivamus metus justo, vulputate ac egestas vitae, condimentum vel erat. Aliquam dui nibh, convallis ac leo eu, commodo gravida est. Quisque arcu nibh, tempor sit amet condimentum quis, euismod eget nisl.</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                Very Truly yours,
                <br><br>
                <hr>
                Juliet Ubarco<br>
                BDC
              </div>
              <div class="col-lg-offset-2 col-lg-2  col-md-2 col-sm-2 col-xs-2 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
                Approved by:
                <br><br>
                <hr>
                SERAFIN R. GARCIA<br>
                VP Marketing
              </div>
              <div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3 col-xs-offset-2 col-xs-3">
                Conforme:<br>
                <hr>
                <hr>
                <hr>
              </div>
            </div>
          </div><!-- /.container -->
          <!-- /content -->
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

  </body>
</html>