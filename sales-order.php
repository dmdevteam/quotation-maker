<?php session_start(); ?>
<?php include 'connect.php';?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Quotation 
    </title>
    <?php include 'header/header-inc.php';?>
    <script src="js/jquery-1.10.2.js"></script>
  <script src="js/tablesorter/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="js/tablesorter/tables.js"></script>
  </head>
  <body>
    <div id="wrapper">
    	
    
      <?php include 'header/header-user.php'; ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h1>Quotation List</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="icon-file-alt"></i> Quotation List</li>
            </ol>
          </div>
          <div class="col-lg-offset-1 col-lg-10">
          
          </div>
          <div class="col-lg-offset-1 col-lg-10">
          	<form class="form-horizontal" role="form" method="post" action="sales-order.php">    		
          		<div class="form-group">
          			<div class="col-sm-12">
          				<div class="form-group">
	          				<div class="col-sm-6">
		        					<label class="control-label">
		        						From:
		        					</label>
		        					<div class="form-group">
				          			<div class="col-sm-4">
				          				<select name="imonth" class="form-control">
				          					<option value="01">January</option>
				          					<option value="02">February</option>
				          					<option value="03">March</option>
				          					<option value="04">April</option>
				          					<option value="05">May</option>
				          					<option value="06">June</option>
				          					<option value="07">July</option>
				          					<option value="08">August</option>
				          					<option value="09">September</option>
				          					<option value="10">October</option>
				          					<option value="11">November</option>
				          					<option value="12">December</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="iday" class="form-control">
				          					<option value="01">1</option>
				          					<option value="02">2</option>
				          					<option value="03">3</option>
				          					<option value="04">4</option>
				          					<option value="05">5</option>
				          					<option value="06">6</option>
				          					<option value="07">7</option>
				          					<option value="08">8</option>
				          					<option value="09">9</option>
				          					<option value="10">10</option>
				          					<option value="11">11</option>
				          					<option value="12">12</option>

				          					<option value="13">13</option>
				          					<option value="14">14</option>
				          					<option value="15">15</option>
				          					<option value="16">16</option>
				          					<option value="17">17</option>
				          					<option value="18">18</option>
				          					<option value="19">19</option>
				          					<option value="20">20</option>
				          					<option value="21">21</option>
				          					<option value="22">22</option>
				          					<option value="23">23</option>


				          					<option value="24">24</option>
				          					<option value="25">25</option>
				          					<option value="26">26</option>
				          					<option value="27">27</option>
				          					<option value="28">28</option>

				          					<option value="29">29</option>
				          					<option value="30">30</option>
				          					<option value="31">31</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="iyear" class="form-control">
				          					<option value="2014">2014</option>
				          					<option value="2015">2015</option>
				          					<option value="2016">2016</option>
				          					<option value="2017">2017</option>
				          				</select>
				          			</div>
		        					</div>
	          				</div>
	          				<div class="col-sm-6">
		        					<label class="control-label">
		        						Until:	
		        					</label>
		        					<div class="form-group">
		          					<div class="col-sm-4">
				          				<select name="vmonth" class="form-control">
				          					<option value="01">January</option>
				          					<option value="02">February</option>
				          					<option value="03">March</option>
				          					<option value="04">April</option>
				          					<option value="05">May</option>
				          					<option value="06">June</option>
				          					<option value="07">July</option>
				          					<option value="08">August</option>
				          					<option value="09">September</option>
				          					<option value="10">October</option>
				          					<option value="11">November</option>
				          					<option value="12">December</option>
				          				</select>
				          			</div>	
				          			<div class="col-sm-4">
				          				<select name="vday" class="form-control">
				          					<option value="01">1</option>
				          					<option value="02">2</option>
				          					<option value="03">3</option>
				          					<option value="04">4</option>
				          					<option value="05">5</option>
				          					<option value="06">6</option>
				          					<option value="07">7</option>
				          					<option value="08">8</option>
				          					<option value="09">9</option>
				          					<option value="10">10</option>
				          					<option value="11">11</option>
				          					<option value="12">12</option>

				          					<option value="13">13</option>
				          					<option value="14">14</option>
				          					<option value="15">15</option>
				          					<option value="16">16</option>
				          					<option value="17">17</option>
				          					<option value="18">18</option>
				          					<option value="19">19</option>
				          					<option value="20">20</option>
				          					<option value="21">21</option>
				          					<option value="22">22</option>
				          					<option value="23">23</option>


				          					<option value="24">24</option>
				          					<option value="25">25</option>
				          					<option value="26">26</option>
				          					<option value="27">27</option>
				          					<option value="28">28</option>

				          					<option value="29">29</option>
				          					<option value="30">30</option>
				          					<option value="31">31</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="vyear" class="form-control">

				          					<option value="2014">2014</option>
				          					<option value="2015">2015</option>
				          					<option value="2016">2016</option>
				          					<option value="2017">2017</option>
				          				</select>
				          			</div>
		        					</div>
	          				</div>
          				</div>
          			</div>
          		</div>

          		<div class="form-group">
          			<div class="col-sm-10">
          				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Show Quotation List</button>
          			</div>
          		</div>
		      <?php if(isset($_POST['pono']))
		          	{
		          		$pono = ($_POST['pono']);
		          		
		          		$qid = ($_POST['id']);
		          		echo "$pono $qid";
		          		date_default_timezone_set('Asia/Manila');
		          		$today = date("Y-m-d");
		          		mysql_query("UPDATE qm_quotation SET qm_status='SalesConverted',qm_pono='$pono' Where qm_quotation_id='$qid';");
		          	        mysql_query( "insert into qm_sales_order(qm_quotation_id,qm_date) values('$qid','$today');" );
		          		echo "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button><i class='fa fa-check-circle'></i> Convertion Succesful</div>";
		          	}

		          	?>
							<div class="form-group">
          			<div class="col-sm-12">
          				<hr>
          			</div>
          		</div>
          	</form>
          </div><!--/.col-lg-5-->
        </div><!-- /.row -->
        <div class="row">
        	<div class="col-lg-offset-1 col-lg-10">
        	
        		<?php
	        		$user= $_SESSION['email'];
	        		$sessionemail = $_SESSION['email'];
	        		$imonth = ($_POST['imonth']);
	        		$iday = ($_POST['iday']);
	        		$iyear = ($_POST['iyear']);

	        		$vmonth = ($_POST['vmonth']);
	        		$vday = ($_POST['vday']);
	        		$vyear = ($_POST['vyear']);



	        		

	        		$start ="$iyear-$imonth-$iday";
	        		$end ="$vyear-$vmonth-$vday";
	        		

	        		$queryy = mysql_query ("SELECT * FROM qm_quotation  WHERE qm_usercreated ='$user' AND qm_date >= '$start' AND qm_date <= '$end' AND qm_status='' ORDER BY `qm_quotation_id` ASC ");
	        		$numrowss = mysql_num_rows($queryy);
	        		if ($numrowss !=0)
	        		{
							$start2 ="$imonth/$iday/$iyear";
	        		$end2 ="$vmonth/$vday/$vyear";
	        		echo "<ul class='nav nav-tabs'><li class='active'><a>$start2 - $end2</a></li></ul>";

	        			echo "<div class='table-responsive'><table class='table table-bordered table-striped table-hover tablesorter'>";
	        			echo "
	        			<thead>
							<tr>
							<th class='header'>Quotation ID <i class='fa fa-sort'></i></th>
	        				<th class='header'>Project Name <i class='fa fa-sort'></i></th>
	        				<th class='header'>Date <i class='fa fa-sort'></i></th>
	        				<th class='header'>Initial Approval by</th>
	        				<th class='header'>Final Approval by</th>
	        				<th class='header'>P.O. No.</th>
	        				<th></th>
							</tr>
	        			</thead>
	        				";
	        			while ($row = mysql_fetch_assoc($queryy))
	        			{
	        			
	        				$id=$row ['qm_quotation_id'];
	        				$pname=$row ['qm_project_name'];
	        				$date=$row ['qm_date'];
	        				$approveby=$row ['qm_approveby'];
						$admin1=$row ['qm_admin1'];
	        				$admin2=$row ['qm_admin2'];


	        			
	        					if($approveby==2 || $approveby==3)
	        					{
	        					echo "	
	        					<form id='contact-form' class='form-horizontal' role='form' method='post' action='sales-order.php'>
	        					<tr class='success'>
	        						<td>$id</td>
	        						<td>$pname</td>
	        						<td>$date</td>
	        						<td>$admin1</td>
	        						<td>$admin2</td>
	        						<td><input type='text' name='pono' class='form-control' id='inputprice' placeholder ='P.O. no Here'> </td>
	        						<td>
	        						 <div class='form-group'>
									<div class='col-sm-12'>
							<button  name='id' type='submit' value='$id' class='btn btn-primary btn-block'>Convert to Sales</button>
									</form></div>
								</div>
	        						</td>
	        					</tr>";
	        					}else
	        					{echo "
	        					<tr 
	        					  data-trigger='hover' 
	        					    data-toggle='tooltip' 
	        					    data-placement='top' 
	        					    title='This Quotation must be approved before converting to Sales'
	        					   >
	        						<td>$id</td>
	        						<td>$pname</td>
	        						<td>$date</td>
	        						<td>$admin1</td>
	        						<td>$admin2</td>
	        						<td><input type='text' class='form-control' id='inputprice' type='text' name='pono' placeholder ='P.O. no Here'> </td>
	        					 <td>
	        					
	        					   
	        					  <div class='form-group'>
									<div class='col-sm-12'>
							<button type='submit' name='id' value='$id' class='btn btn-primary btn-block disabled'>Convert to Sales</button>
									</div>
								</div>
								
	        					   </td></tr>";}
	        			}
	        			echo "</table></div>";
	        	 	}
        		?>
        		
        	</div>
        </div>
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    <!-- JavaScript -->
    <script src="js/bootstrap.js"></script>
    <script>
    $().alert()
    	$('#the-button-disabled').tooltip()
    	 $(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
    </script>
  </body>
</html>