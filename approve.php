 		   
<?php include 'connect.php';?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Approval of Quotation</title>
    <?php include 'header/header-inc.php';?>
  </head>
  <body>
    <div id="wrapper">
      <?php include 'header/header-admin.php'; ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h1>Approval of Quotation</h1>
            <ol class="breadcrumb">
              <li class="active">Approval of Quotation</li>
            </ol>
          </div>
          <div class="col-lg-offset-1 col-lg-10"><!-- wrapper column-->
            <!-- content -->
            <div class="row">
              <div class="col-lg-12">
                
                  <?php

                    if(isset($_GET['approve']))
                    {

                      $approve = ($_GET['approve']);
                      if($id=='admin.gx')
                      {
                        mysql_query("UPDATE qm_quotation SET qm_approveby='3',qm_admin1='GX',qm_admin2='GX' Where qm_quotation_id='$approve';");
                      }
                      else
                      {

                        $queryy = mysql_query("SELECT * FROM qm_quotation where qm_quotation_id='$approve' and qm_approveby='1'");
                        $numrowss = mysql_num_rows($queryy);
                        if ($numrowss !=0)
                        {   
                         while ($row = mysql_fetch_assoc($queryy))
                         {
                          mysql_query("UPDATE qm_quotation SET qm_approveby='2',qm_admin2='$fname' Where qm_quotation_id='$approve';");
                          }
                        }   
                      
                        $queryy = mysql_query("SELECT * FROM qm_quotation where qm_quotation_id='$approve' and qm_approveby=''");
                        $numrowss = mysql_num_rows($queryy);
                        if ($numrowss !=0)
                        {   
                         while ($row = mysql_fetch_assoc($queryy))
                         {
                          mysql_query("UPDATE qm_quotation SET qm_approveby='1',qm_admin1='$fname' Where qm_quotation_id='$approve';");
                          }
                        }     
                      }

                      echo "<div class='alert alert-success alert-dismissable'>
                       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <i class='fa fa-check-circle'></i> You have successfully Approved <b>'Quotation ID No. $approve'</b>  </span></div><!--/close notif -->";
                    }
                  ?>    
              </div>
              <div class="col-lg-6">
                <div class="list-group">
                  <a href="#" class="list-group-item list-group-item-info active">
                     <h4 class="list-group-item-heading"><i class="fa fa-question-circle"></i> Quotations for Initial Approval</h4>
                  </a>
                  <?php
                    if($pending_quotation>=1)
                    {
                      $queryy = mysql_query("SELECT * FROM qm_quotation where qm_approveby=''");
                      $numrowss = mysql_num_rows($queryy);
                      if ($numrowss !=0)
                      {   
                       while ($row = mysql_fetch_assoc($queryy))
                        {
                         $id=$row ['qm_quotation_id'];
                         $pname=$row ['qm_project_name'];
                         echo "<a class='list-group-item' href='approve.php?approve=$id'>$id - <strong>$pname</strong> <i class='pull-right fa fa-thumbs-up'></i></a>";
                         echo"<a class='list-group-item' href='qprint.php?id=$id'>View Quotation</a>";
                        }
                      }
                    }
                    else {
                      echo "<li class='list-group-item list-group-info'><strong>No Pending Quotation</strong></li>";
                    }  
                  ?> 
                </div><!-- /.list-group -->
              </div>
              <!-- /For 2nd Approval -->
               <div class="col-lg-6">
                    <div class="list-group">
                      <a href="#" class="list-group-item list-group-item-info active">
                       <h4 class="list-group-item-heading">
                        <i class="fa fa-question-circle"></i> Quotations for Final Approval
                       </h4>
                      </a>
                      <?php

                        if($pending_quotation>=1)
                        {
                          $queryy = mysql_query("SELECT * FROM qm_quotation where qm_approveby='1' and qm_admin1 !='$fname'");
                          $numrowss = mysql_num_rows($queryy);
                          if ($numrowss !=0)
                          {   
                           while ($row = mysql_fetch_assoc($queryy))
                           {
                             $id=$row ['qm_quotation_id'];
                             $pname=$row ['qm_project_name'];
                             echo "<a class='list-group-item' href='approve.php?approve=$id'>$id - <strong>$pname</strong> <i class='pull-right fa fa-thumbs-up'></i></a>";
                           }
                          }
                        }
                        else
                        {
                           echo "<li class='list-group-item list-group-info'><strong>No Pending Quotation</strong></li>";                        
                        }
                      ?> 
                    </div>
              </div><!-- /.col --><!-- /For 2nd Approval -->
            </div><!-- /.row -->
          </div><!-- /.col wrapper column -->
          
          
          
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    
    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>
    $('#the_approval').tooltip(options)
      $(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
    </script>
  </body>
</html>

   