<?php include 'connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>

	<title>Add Item</title>
	<?php include 'header/header-inc.php';?>
</head>
<body>
	<div id="wrapper">

		<?php include 'header/header-admin.php'; ?>

		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1>Add Item</h1>
					<ol class="breadcrumb">
						<li class="active"><i class="icon-file-alt"></i> Add Item</li>
					</ol>
				</div>
				<div class="col-lg-5 col-lg-offset-1">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-plus-square"></i> Add Item</h3>
						</div>
						<div class="panel-body">
							<form id="contact-form" class="form-horizontal" role="form" method="post" action="add-item.php">
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="itemname" placeholder ="Item Name here" class="form-control" id="inputName">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" name="category" placeholder ="Category here" class="form-control" id="inputName">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-6 col-lg-6">
										<input type="text" class="form-control" id="inputModel" type="text" name="model" placeholder="Enter Model here ">
									</div>
									<div class="col-sm-6 col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">&#8369;</span>
											<input type="text" class="form-control" id="inputprice" type="text" name="price" placeholder ="Price here">
										</div>
									</div>
								</div>
								<div class="form-group">
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<textarea class="form-control" id="inputdesc" placeholder="Description" name="desc" rows="4" cols="50"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary btn-block">Register Item</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
				</div>
<?php
if(isset($_POST['itemname'])){
include 'connect.php';
	$itemname = ($_POST['itemname']);
	$model = ($_POST['model']);
	$desc = ($_POST['desc']);
	$price = ($_POST['price']);
	$category = ($_POST['category']);
	mysql_query("insert into qm_item (qm_item_name,qm_model,qm_description,qm_price,qm_category)								values('$itemname','$model','$desc','$price','$category ');");	

	echo "
	<div class='col-lg-6 col-md-6 col-xs-12 col-sm-12'>
	  <div class='alert alert-success alert-dismissable'>
	    <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
	    <i class='fa fa-check-circle'></i> Successful
	   </div>
	 </div>";
}							
?>
			</div><!-- /.row -->

		</div><!-- /#page-wrapper -->

	</div><!-- /#wrapper -->

	<!-- JavaScript -->
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.validate.min.js"></script>
<script>
	$(document).ready(function(){
 
     $('#contact-form').validate(
     {
      rules: {
      	itemname: {
          minlength: 2,
          required: false
        },

        model: {
          required: false,
          minlength: 2
        },

        price: {
          required: false,
          minlength: 2
        },
        desc: {
          minlength: 2,
          required: false
        }
      },
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
      },
      success: function(element) {
        element
        .closest('.form-group').removeClass('has-error').addClass('has-success');
      }
    });
}); // end document.ready

	$(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
</script>
</body>
</html>