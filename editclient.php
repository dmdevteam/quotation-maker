<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>EDIT CLIENT</title>
  <?php include 'connect.php'; ?>
  <?php include 'header/header-inc.php';?>


  <script type="text/javascript">
  function enableButton() {
    if(document.getElementById('option').checked){
      document.getElementById('b1').disabled='';
    } else {
      document.getElementById('b1').disabled='true';
    }
  }
  </script>
</head>
<body>
  <div id="wrapper">
       <?php 
  $admin =  $_SESSION['level'];
if ($admin ==1)
{
 include 'header/header-admin.php'; 
  
}
else{
 include 'header/header-user.php';
  }
  
  ?>

  <div id="page-wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h1>Client Registration</h1>
        <ol class="breadcrumb">
          <li class="active"><i class="icon-file-alt"></i> EDIT CLIENT</li>
        </ol>
      </div>
      <div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5  col-sm-12 col-xs-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><i class="fa fa-question-circle"></i> EDIT CLIENT</div>
            <div class="panel-body">
            <?php
						 $dclient = ($_POST['dclient']);
	          			 if(isset($dclient)) 
	          			 {
	          		
					mysql_query("DELETE FROM qm_client WHERE qm_name='$dclient'");
	          			 }
						?>
            <?php
						 $client = ($_POST['eclient']);
						 if(isset($client)){ ?>
						  <?php
	          						$queryy = mysql_query("SELECT * FROM qm_client WHERE qm_name='$client'");
	          						$numrowss = mysql_num_rows($queryy);
	          						if ($numrowss !=0)
	          						{   
	          							while ($row = mysql_fetch_assoc($queryy))
	          							{
	          								$clientid=$row ['qm_client_id'];
	          								$name=$row ['qm_name'];
	          								$address=$row ['qm_address'];
	          								$contact=$row ['qm_contact'];
	          								$faxno=$row ['qm_faxno'];
	          								$person=$row ['qm_cperson'];
	          								
	          							}
	          						}
	          				  ?>
              <form id="contact-form" class="form-horizontal" role="form" method="post" action="editclient.php">
              <input type="hidden" name="oldname" value=<?php echo $name; ?>>
                  <input type="hidden" name="client" value=<?php echo $clientid; ?>>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-copyright-mark"></span>
                      </span>
                   
                         <textarea size="3" name="companyname" class="form-control" id="inputCompanyname">  <?php echo $name; ?>
                      </textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12" data-toggle="tooltip" data-placement="top" title="Enter your Company Address here." 
                           data-trigger="hover" id="client-address">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-map-marker"></span>
                      </span>
                      <textarea size="3" name="address" class="form-control" id="inputAddress">  <?php echo $address; ?>
                      </textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                      </span>
                   
                      <textarea size="3" name="contactperson" class="form-control" id="inputContactPerson">  <?php echo $person; ?>
                      </textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-earphone"></span>
                      </span>
                      <input type="text" name="contact" class="form-control" id="inputContact"  value=<?php echo $contact; ?>>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">  
                        <span class="glyphicon glyphicon-phone-alt"></span>
                      </span>
                      
                         <textarea size="3" name="faxno" class="form-control" id="inputFax">  <?php echo $faxno; ?>
                      </textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-1 col-sm-10" data-toggle="tooltip" data-placement="top" title="Agree to Terms and Condition to continue" data-trigger="hover" id="terms-condition">
                   <input type="checkbox" type="checkbox"name="option" id="option" onclick="javascript:enableButton();">
                   I agree to the <a href="">Terms and Conditions</a>
                 </div>
               </div>
                <button type="submit" class="btn btn-primary btn-block"  name = "b1" id="b1"  onclick="form.action='registration.ph'method='post' ;" disabled>Sign-up</button>
              </form>
              <?php }	?>
              	<?php
	          			 $eclient = ($_POST['eclient']);
	          			 if(empty($eclient)) 
	          			 {
	          			 
	          			 ?>
              	<form id="contact-form" class="form-horizontal" role="form" method="post" action="editclient.php">
							<div class="form-group">
	          				<div class="col-sm-12">
	          					<label>Client Name</label>
	          					<select name="eclient" id="client" class="form-control">
	          						<?php
	          						$queryy = mysql_query("SELECT * FROM qm_client ORDER BY qm_name ASC");
	          						$numrowss = mysql_num_rows($queryy);
	          						if ($numrowss !=0)
	          						{   
	          							while ($row = mysql_fetch_assoc($queryy))
	          							{
	          								
	          								$client=$row ['qm_name'];
	          								echo "<option value='$client'>$client</option>";
	          							}
	          						}
	          						?>				
	          					</select>
	          					
	          				</div>
	          			</div>
	          			<button type="submit" value="try" class="btn btn-default btn-block">Edit Item</button>
	          			</form>
	          			
              	<form id="contact-form" class="form-horizontal" role="form" method="post" action="editclient.php">
							<div class="form-group">
	          				<div class="col-sm-12">
	          					<label>Client Name</label>
	          					<select name="dclient" id="client" class="form-control">
	          						<?php
	          						$queryy = mysql_query("SELECT * FROM qm_client ORDER BY qm_name ASC");
	          						$numrowss = mysql_num_rows($queryy);
	          						if ($numrowss !=0)
	          						{   
	          							while ($row = mysql_fetch_assoc($queryy))
	          							{
	          								
	          								$client=$row ['qm_name'];
	          								echo "<option value='$client'>$client</option>";
	          							}
	          						}
	          						?>				
	          					</select>
	          					
	          				</div>
	          			</div>
	          			<button type="submit" value="try" class="btn btn-danger btn-block">Delete Item</button>
	          			</form>
	          			<?php } ?>
          </div><!--/.panel-body -->
        </div><!--/.panel-primary -->
        <!--start -->
        <!--php connection -->
        <?php
        if(isset($_POST['companyname'])){

         $name = ($_POST['companyname']);
         $city = ($_POST['address']);
         $email = ($_POST['contactperson']);
         $contact = ($_POST['contact']);
         $fax = ($_POST['faxno']);

         $errors = array();
        if(!$name){
          $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Name is not defined!</div";
        }
        if(!$city){
          $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Address is not defined!</div>";
        }
       

        if(count($errors) > 0){
           echo "
              </div>
              <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              
              ";

          foreach($errors AS $error){
            echo "$error";
          }
           echo "</div>";
        } else {
          echo "</div>
              <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>";
      //for the timecode
  //end
  //end
  
  	  $clientid= ($_POST['client']);
  	  $oldname= ($_POST['oldname']);
         $clientname = ($_POST['companyname']);
         $clientcity = ($_POST['address']);
         $clientperson = ($_POST['contactperson']);
         $clientcontact = ($_POST['contact']);
         $clientfax = ($_POST['faxno']);

if(!empty($clientname))
{
mysql_query("UPDATE qm_client SET qm_name='$clientname' Where qm_client_id='$clientid';");
}
if(!empty( $clientcity))
{
mysql_query("UPDATE qm_client SET qm_address='$clientcity' Where qm_client_id='$clientid';");
}
if(!empty($clientcontact))
{
mysql_query("UPDATE qm_client SET qm_contact='$clientcontact' Where qm_client_id='$clientid';");
}
if(!empty($clientperson))
{
mysql_query("UPDATE qm_client SET qm_cperson='$clientperson' Where qm_client_id='$clientid';");
}
if(!empty($clientfax))
{
mysql_query("UPDATE qm_client SET qm_faxno='$clientfax' Where qm_client_id='$clientid';");
}

          
          echo "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-check-circle'></i> You have successfully Edited </span></div>";
        }
      }
      else {}
      ?>
      <!--ending -->
      <!--</div>-->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script>
   $('#terms-condition').tooltip()
   $('#client-address').tooltip()

   $(document).ready(function(){
 
     $('#contact-form').validate(
     {
      rules: {
        companyname: {
          minlength: 2,
          required: false
        },

        address: {
          required: false,
          minlength: 2
        },

        contactperson: {
          required: false,
          minlength: 2
        },
        contact: {
          minlength: 2,
          required: false
        },
        faxno: {
          minlength: 2,
          required: false
        }
      },
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
      },
      success: function(element) {
        element
        .closest('.form-group').removeClass('has-error').addClass('has-success');
      }
    });
}); // end document.ready

   $(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
</script>
</body>
</html>