<?php session_start(); ?>
<?php include 'connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Quotation</title>
    <?php include 'header/header-inc.php';?>

  </head>
  <body>
    <div id="wrapper">
     <?php include 'header/header-user.php';?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
            <h1>Create Quotation</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="icon-file-alt"></i> Create Quotation</li>
            </ol>
          </div>
          <div class="col-sm-offset-1 col-sm-5">
          	<?php
	          		if(isset($_POST['pname'])){
	          			unset($_SESSION['quotationid']); 
						// i= Inquiry v=Validty
	          			$projectname = ($_POST['pname']);
	          			$client = ($_POST['client']);

	          			$imonth = ($_POST['imonth']);
	          			$iday = ($_POST['iday']);
	          			$iyear = ($_POST['iyear']);

	          			$vmonth = ($_POST['vmonth']);
	          			$vday = ($_POST['vday']);
	          			$vyear = ($_POST['vyear']);

	          			$top = ($_POST['top']);
					$others = ($_POST['others']);

	          			$note = ($_POST['note']);
	          			$validity = ($_POST['validity']);
	          			$today = date("Y-m-d");
	          			$user = $_SESSION['email'];
	          			mysql_query("insert into qm_quotation (qm_project_name,qm_clientinfo,qm_terms,qm_usercreated,qm_imonth,qm_iday,qm_iyear,qm_vmonth,qm_vday,qm_vyear,qm_validity,qm_date,qm_note)								values('$projectname','$client','$top $others','$user','$imonth','$iday','$iyear','$vmonth','$vday','$vyear','$validity','$today','$note');");	
	          			
	          			$nextid = mysql_insert_id();
	          			

	          			$_SESSION['quotationid']=$nextid;
	          			
	          			echo "
						
						<div class='alert alert-success alert-dismissable'>
						  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
	          			  <i class='fa fa-check-circle'></i> <b>Success</b> Proceed to <a href ='qitem.php' class='alert alert-link'>next step<i class='fa fa-angle-double-right'></i></a></div>";
	          		}							
	          		?>
	          <div class="panel panel-primary">
	          	<div class="panel-heading">
	          		<h3 class="panel-title">
	          			<i class="fa fa-edit"></i>
	          			Create Quotation
	          		</h3>
	          	</div>
	          	<div class="panel-body">
	          		<form class="form-horizontal" role="form" method="post" action="quotation.php">
	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<input type="text" class="form-control" name="pname" id="input" placeholder="Project Name">
	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<label>Client</label>
	          					<select name="client" id="client" class="form-control">
	          						<?php
	          						$queryy = mysql_query("SELECT * FROM qm_client");
	          						$numrowss = mysql_num_rows($queryy);
	          						if ($numrowss !=0)
	          						{   
	          							while ($row = mysql_fetch_assoc($queryy))
	          							{
	          								$clientid=$row ['qm_client_id'];
	          								$name=$row ['qm_name'];
	          								echo "<option value='$clientid'>$name</option>";
	          							}
	          						}
	          						?>				
	          					</select>
	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<hr>
	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">
		          				<label for="">
		          					Date of Inquiry
		          				</label>
	          				</div>
	          				<div class="col-sm-4">
	          					<label>Month</label>
	          					<select name="imonth" class="form-control">
	          						<option value="01">January</option>
	          						<option value="02">February</option>
	          						<option value="03">March</option>
	          						<option value="04">April</option>
	          						<option value="05">May</option>
	          						<option value="06">June</option>
	          						<option value="07">July</option>
	          						<option value="08">August</option>
	          						<option value="09">September</option>
	          						<option value="10">October</option>
	          						<option value="11">November</option>
	          						<option value="12">December</option>
	          					</select>
	          				</div>
	          				<div class="col-sm-4">
	          					<label>Day</label>
	          					<select name="iday" class="form-control">
	          						<option value="01">1</option>
	          						<option value="02">2</option>
	          						<option value="03">3</option>
	          						<option value="04">4</option>
	          						<option value="05">5</option>
	          						<option value="06">6</option>
	          						<option value="07">7</option>
	          						<option value="08">8</option>
	          						<option value="09">9</option>
	          						<option value="10">10</option>
	          						<option value="11">11</option>
	          						<option value="12">12</option>

	          						<option value="13">13</option>
	          						<option value="14">14</option>
	          						<option value="15">15</option>
	          						<option value="16">16</option>
	          						<option value="17">17</option>
	          						<option value="18">18</option>
	          						<option value="19">19</option>
	          						<option value="20">20</option>
	          						<option value="21">21</option>
	          						<option value="22">22</option>
	          						<option value="23">23</option>


	          						<option value="24">24</option>
	          						<option value="25">25</option>
	          						<option value="26">26</option>
	          						<option value="27">27</option>
	          						<option value="28">28</option>

	          						<option value="29">29</option>
	          						<option value="30">30</option>
	          						<option value="31">31</option>
	          					</select>
	          				</div>
	          				<div class="col-sm-4">
	          					<label>Year</label>
	          					<select name="iyear" class="form-control">
	          						
	          						<option value="2014">2014</option>
	          						<option value="2015">2015</option>
	          						<option value="2016">2016</option>
	          					</select>
	          				</div>
	          			</div>

	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<label>
	          						Date of Validity:	
	          					</label>
	          				</div>
	          				<div class="col-sm-4">
	          					<label>Month</label>
	          					<select name="vmonth" class="form-control">
	          						<option value="01">January</option>
	          						<option value="02">February</option>
	          						<option value="03">March</option>
	          						<option value="04">April</option>
	          						<option value="05">May</option>
	          						<option value="06">June</option>
	          						<option value="07">July</option>
	          						<option value="08">August</option>
	          						<option value="09">September</option>
	          						<option value="10">October</option>
	          						<option value="11">November</option>
	          						<option value="12">December</option>
	          					</select>
	          				</div>	
										<div class="col-sm-4">
	          					<label>Day</label>
	          					<select name="vday" class="form-control">
	          						<option value="01">1</option>
	          						<option value="02">2</option>
	          						<option value="03">3</option>
	          						<option value="04">4</option>
	          						<option value="05">5</option>
	          						<option value="06">6</option>
	          						<option value="07">7</option>
	          						<option value="08">8</option>
	          						<option value="09">9</option>
	          						<option value="10">10</option>
	          						<option value="11">11</option>
	          						<option value="12">12</option>

	          						<option value="13">13</option>
	          						<option value="14">14</option>
	          						<option value="15">15</option>
	          						<option value="16">16</option>
	          						<option value="17">17</option>
	          						<option value="18">18</option>
	          						<option value="19">19</option>
	          						<option value="20">20</option>
	          						<option value="21">21</option>
	          						<option value="22">22</option>
	          						<option value="23">23</option>


	          						<option value="24">24</option>
	          						<option value="25">25</option>
	          						<option value="26">26</option>
	          						<option value="27">27</option>
	          						<option value="28">28</option>

	          						<option value="29">29</option>
	          						<option value="30">30</option>
	          						<option value="31">31</option>
	          					</select>
	          				</div>
										<div class="col-sm-4">
	          					<label>Year</label>
	          					<select name="vyear" class="form-control">
	          						
	          						<option value="2014">2014</option>
	          						<option value="2015">2015</option>
	          						<option value="2016">2016</option>
	          					</select>
	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<hr>
	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">	
	          					Terms of Payment
	          					<div class="radio">
	          						<label>
	          							<input type="radio" name="top" value="Cash on Delivery" onclick="javascript:yesnoCheck();"  id="noCheck" checked> Cash on Delivery
	          						</label>
	          					</div>
	          					<div class="radio">
	          						<label>
	          							<input type="radio" name="top" value="Full Payment" onclick="javascript:yesnoCheck();"  id="noCheck"> Full Payment
	          						</label>
	          					</div>
	          					<div class="radio">
	          						<label>
	          						<input type="radio" name="top" value="50% D.P. 50%upon  Completion" onclick="javascript:yesnoCheck();"  id="noCheck"> 50% Down Payment | 50%upon  Completion
	          						</label>
	          					</div>	
	          								
	          					Others:<input type="radio" onclick="javascript:yesnoCheck();" name="top" value="" id="yesCheck"> 
    <div id="ifYes" style="display:none">
       <input type='text' id='yes' name='others'><br>
      
    </div>
     


	          				</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-12">
	          					<input type="text" name="validity" class="form-control" id="input" placeholder="Validity : Client Info">
	          				</div>
	          			</div>
	          			<div class="form-group">
	          			<div class="checkbox">
	          			<div class="col-sm-12">
	          		<input type="checkbox" name="note" value="1"> Note for Repair<br><br>
	          			</div>
	          			</div>
	          			<div class="form-group">
	          				<div class="col-sm-offset-1 col-sm-10">
	          					<button type="submit" class="btn btn-primary">Register Item</button>
	          				</div>
	          			</div>
	          		</form>
	          		
	          	</div>
	          </div> 
          </div><!-- /.col-sm-6 -->
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
  <script type="text/javascript">

function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'block';
    }
    else document.getElementById('ifYes').style.display = 'none';

}

</script>
  </body>
</html>