<?php session_start(); ?>
<?php include 'connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Sales List</title>
    <?php include 'header/header-inc.php';?>
    <script src="js/jquery-1.10.2.js"></script>
  <script src="js/tablesorter/jquery.tablesorter.js"></script>
  <script type="text/javascript" src="js/tablesorter/tables.js"></script>
  </head>
  <body>
    <div id="wrapper">

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12 hidden-print">
            <h1>Sales List</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="icon-file-alt"></i> Sales List</li>
            </ol>
          </div>
          <div class="col-lg-offset-1 col-lg-10">
          	<form class="form-horizontal" role="form" method="post" action="sales-list.php">    		
          		<div class="form-group hidden-print">
          			<div class="col-sm-12">
          				<div class="form-group">
	          				<div class="col-sm-6">
		        					<label class="control-label">
		        						From
		        					</label>
		        					<div class="form-group">
				          			<div class="col-sm-4">
				          				<select name="imonth" class="form-control">
				          					<option value="01">January</option>
				          					<option value="02">February</option>
				          					<option value="03">March</option>
				          					<option value="04">April</option>
				          					<option value="05">May</option>
				          					<option value="06">June</option>
				          					<option value="07">July</option>
				          					<option value="08">August</option>
				          					<option value="09">September</option>
				          					<option value="10">October</option>
				          					<option value="11">November</option>
				          					<option value="12">December</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="iday" class="form-control">
				          					<option value="01">1</option>
				          					<option value="02">2</option>
				          					<option value="03">3</option>
				          					<option value="04">4</option>
				          					<option value="05">5</option>
				          					<option value="06">6</option>
				          					<option value="07">7</option>
				          					<option value="08">8</option>
				          					<option value="09">9</option>
				          					<option value="10">10</option>
				          					<option value="11">11</option>
				          					<option value="12">12</option>

				          					<option value="13">13</option>
				          					<option value="14">14</option>
				          					<option value="15">15</option>
				          					<option value="16">16</option>
				          					<option value="17">17</option>
				          					<option value="18">18</option>
				          					<option value="19">19</option>
				          					<option value="20">20</option>
				          					<option value="21">21</option>
				          					<option value="22">22</option>
				          					<option value="23">23</option>


				          					<option value="24">24</option>
				          					<option value="25">25</option>
				          					<option value="26">26</option>
				          					<option value="27">27</option>
				          					<option value="28">28</option>

				          					<option value="29">29</option>
				          					<option value="30">30</option>
				          					<option value="31">31</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="iyear" class="form-control">
				          					<option value="2014">2014</option>
				          					<option value="2015">2015</option>
				          					<option value="2016">2016</option>
				          					<option value="2017">2017</option>
				          				</select>
				          			</div>
		        					</div>
	          				</div>
	          				<div class="col-sm-6">
		        					<label class="control-label">
		        						To	
		        					</label>
		        					<div class="form-group">
		          					<div class="col-sm-4">
				          				<select name="vmonth" class="form-control">
				          					<option value="01">January</option>
				          					<option value="02">February</option>
				          					<option value="03">March</option>
				          					<option value="04">April</option>
				          					<option value="05">May</option>
				          					<option value="06">June</option>
				          					<option value="07">July</option>
				          					<option value="08">August</option>
				          					<option value="09">September</option>
				          					<option value="10">October</option>
				          					<option value="11">November</option>
				          					<option value="12">December</option>
				          				</select>
				          			</div>	
				          			<div class="col-sm-4">
				          				<select name="vday" class="form-control">
				          					<option value="01">1</option>
				          					<option value="02">2</option>
				          					<option value="03">3</option>
				          					<option value="04">4</option>
				          					<option value="05">5</option>
				          					<option value="06">6</option>
				          					<option value="07">7</option>
				          					<option value="08">8</option>
				          					<option value="09">9</option>
				          					<option value="10">10</option>
				          					<option value="11">11</option>
				          					<option value="12">12</option>

				          					<option value="13">13</option>
				          					<option value="14">14</option>
				          					<option value="15">15</option>
				          					<option value="16">16</option>
				          					<option value="17">17</option>
				          					<option value="18">18</option>
				          					<option value="19">19</option>
				          					<option value="20">20</option>
				          					<option value="21">21</option>
				          					<option value="22">22</option>
				          					<option value="23">23</option>


				          					<option value="24">24</option>
				          					<option value="25">25</option>
				          					<option value="26">26</option>
				          					<option value="27">27</option>
				          					<option value="28">28</option>

				          					<option value="29">29</option>
				          					<option value="30">30</option>
				          					<option value="31">31</option>
				          				</select>
				          			</div>
				          			<div class="col-sm-4">
				          				<select name="vyear" class="form-control">

				          					<option value="2014">2014</option>
				          					<option value="2015">2015</option>
				          					<option value="2016">2016</option>
				          					<option value="2017">2017</option>
				          				</select>
				          			</div>
		        					</div>
	          				</div>
          				</div>
          			</div>
          		</div>

          		<div class="form-group hidden-print">
          			<div class="col-sm-10">
          				<button type="submit" class="btn btn-primary"><i class='fa fa-list'></i> View List</button>
          			</div>
          		</div>
							<div class="form-group">
          			<div class="col-sm-12">
          				<hr>
          			</div>
          		</div>
          	</form>
          </div><!--/.col-lg-5-->
        </div><!-- /.row -->
        <div class="row">
        	<div class="col-lg-offset-1 col-lg-10">
        		<?php
	        		$user= $_SESSION ['email'];
	        		
	        		$imonth = ($_POST['imonth']);
	        		$iday = ($_POST['iday']);
	        		$iyear = ($_POST['iyear']);

	        		$vmonth = ($_POST['vmonth']);
	        		$vday = ($_POST['vday']);
	        		$vyear = ($_POST['vyear']);



	        		

	        		$start ="$iyear-$imonth-$iday";
	        		$end ="$vyear-$vmonth-$vday";
	        		

	        		$queryy = mysql_query ("SELECT * FROM qm_quotation  WHERE qm_date >= '$start' AND qm_date <= '$end' AND qm_status='SalesConverted'ORDER BY `qm_date` DESC ");
	        		$numrowss = mysql_num_rows($queryy);
	        		if ($numrowss !=0)
	        		{
							$start2 ="$imonth/$iday/$iyear";
	        		$end2 ="$vmonth/$vday/$vyear";
	        		echo "
	        		<div class='visible-print page-header'>
	        		<h1>Sales List <small>from <b>$start2</b> to <b>$end2</b> </small></h1>
	        		</div>
	        		";
	        		echo "<ul class='nav nav-tabs hidden-print'><li class='active'><a><h4>Sales List From <b>$start2</b> to <b>$end2</b></h4></a></li></ul>";

	        			echo "<div class='table-responsive'><table class='table table-bordered  table-hover tablesorter'>";
	        			echo "
	        			<thead>
							<tr>
		        				<th>ID <i class='fa fa-sort'></i></th>
		        				<th>Project Name <i class='fa fa-sort'></i></th>
		        				<th>Date <i class='fa fa-sort'></i></th>
		        				<th>Initial Approval</th>
		        				<th>Final Approval</th>
		        				<th></th>
							</tr>
	        			</thead>
	        			<tbody>
	        				";
	        			while ($row = mysql_fetch_assoc($queryy))
	        			{
	        				$id=$row ['qm_quotation_id'];
	        				$pname=$row ['qm_project_name'];
	        				$date=$row ['qm_date'];
	        				$approveby=$row ['qm_approveby'];
	        				$admin1=$row ['qm_admin1'];
	        				$admin2=$row ['qm_admin2'];

						echo "
	        					<tr >
	        						<td>$id</td>
	        						<td>$pname</td>
	        						<td>$date</td>
	        						<td>First Sale</td>
	        						<td>Second Sale</td>
	        						<td><a href ='../tcpdf/examples/example_aaa.php?ID=$id'' target='_blank'  class='btn btn-default'><i class='fa fa-print'></i> Print</td>
	        					</tr>";
						
						
			
						
	        				


	        			
	        			}
	        			echo "</tbody></table></div>";
	        		}
        		?>
        		    <?php 
  $admin =  $_SESSION['level'];
if ($admin ==1)
{
 include 'header/header-admin.php';

}
else{
  include 'header/header-user.php';  
  }
  
  ?>
        	</div>
        </div>
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    <!-- JavaScript -->
    <script src="js/bootstrap.js"></script>
  </body>
</html>