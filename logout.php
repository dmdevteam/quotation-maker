<?php session_start(); session_destroy();?>
<?php include 'header/header-inc.php';?>

<div class="container">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<div class="alert alert-success">
				You have been successfully logged out
			</div>
		</div>
	</div>
</div>