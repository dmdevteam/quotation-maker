 <!DOCTYPE html>
<html lang="en">
  <head>
    <title>User Login</title>
    <?php include '../header/header-inc.php';?>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h1 class="panel-title"><i class="fa fa-quote-left"></i> Quotation Maker - Admin</h1>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" action="home-admin.php" method = 'POST' autocomplete="on">
                    <div class="form-group">
                        <label for="username" class="uname col-lg-2 control-label" data-icon="u" class="col-lg-2 control-label">Email:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                                <input type="text" class="form-control" id="username"  name="username" required="required" type="email" placeholder="myusername or mymail@mail.com">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword1" class="youpasswd col-lg-2 control-label" data-icon="p">Password:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </span>
                                <input name="password" required="required" type="password" class="form-control" id="inputPassword1" placeholder="eg. X8df!90EO">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-primary btn-block">Log in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
      </div>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>
