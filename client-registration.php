<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>Client Registration</title>
  <?php include 'connect.php'; ?>
  <?php include 'header/header-inc.php';?>


  <script type="text/javascript">
  function enableButton() {
    if(document.getElementById('option').checked){
      document.getElementById('b1').disabled='';
    } else {
      document.getElementById('b1').disabled='true';
    }
  }
  </script>
</head>
<body>
  <div id="wrapper">
       <?php 
  $admin =  $_SESSION['level'];
if ($admin ==1)
{
 include 'header/header-admin.php'; 
  
}
else{
 include 'header/header-user.php';
  }
  
  ?>

  <div id="page-wrapper">

    <div class="row">
      <div class="col-lg-12">
        <h1>Client Registration</h1>
        <ol class="breadcrumb">
          <li class="active"><i class="icon-file-alt"></i> Client Registration</li>
        </ol>
      </div>
      <div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5  col-sm-12 col-xs-12">
        <div class="panel panel-primary">
          <div class="panel-heading"><i class="fa fa-question-circle"></i> Client Registration</div>
            <div class="panel-body">
              <form id="contact-form" class="form-horizontal" role="form" method="post" action="client-registration.php">
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-copyright-mark"></span>
                      </span>
                      <input type="text" name="companyname" class="form-control" id="inputCompanyname" placeholder="Company Name">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12" data-toggle="tooltip" data-placement="top" title="Enter your Company Address here." 
                           data-trigger="hover" id="client-address">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-map-marker"></span>
                      </span>
                      <textarea size="3" name="address" class="form-control" id="inputAddress">
                      </textarea>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                      </span>
                      <input type="text" name="contactperson" class="form-control" id="inputContactPerson" placeholder="Contact Person">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-earphone"></span>
                      </span>
                      <input type="text" name="contact" class="form-control" id="inputContact" placeholder="Contact Number">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <span class="input-group-addon">  
                        <span class="glyphicon glyphicon-phone-alt"></span>
                      </span>
                      <input type="text" name="faxno" class="form-control" id="inputFax" placeholder="Fax Number">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-1 col-sm-10" data-toggle="tooltip" data-placement="top" title="Agree to Terms and Condition to continue" data-trigger="hover" id="terms-condition">
                   <input type="checkbox" type="checkbox"name="option" id="option" onclick="javascript:enableButton();">
                   I agree to the <a href="">Terms and Conditions</a>
                 </div>
               </div>
                <button type="submit" class="btn btn-primary btn-block"  name = "b1" id="b1"  onclick="form.action='registration.ph'method='post' ;" disabled>Sign-up</button>
              </form>
          </div><!--/.panel-body -->
        </div><!--/.panel-primary -->
        <!--start -->
        <!--php connection -->
        <?php
        if(isset($_POST['companyname'])){

         $name = ($_POST['companyname']);
         $city = ($_POST['address']);
         $email = ($_POST['contactperson']);
         $contact = ($_POST['contact']);
         $fax = ($_POST['faxno']);

         $errors = array();
        if(!$name){
          $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Name is not defined!</div";
        }
        if(!$city){
          $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Address is not defined!</div>";
        }
       

        if(count($errors) > 0){
           echo "
              </div>
              <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              
              ";

          foreach($errors AS $error){
            echo "$error";
          }
           echo "</div>";
        } else {
          echo "</div>
              <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>";
      //for the timecode
  //end
  //end
          $time = time();
          $actual_time = date ('M d Y @H:i:s', $time );

          $sql4 = "INSERT INTO `qm_client`
          (`qm_name`,`qm_address`,`qm_contact`,`qm_faxno`,`qm_cperson`)

          VALUES ('".$name."','".$city."','".$contact."','".($fax)."','".($email)."')";
          $res4 = mysql_query($sql4) or die(mysql_error());
          echo "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-check-circle'></i> You have successfully registered with the email <b> '.$email.'</b></span></div>";
        }
      }
      else {}
      ?>
      <!--ending -->
      <!--</div>-->
    </div><!-- /.row -->
  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script>
   $('#terms-condition').tooltip()
   $('#client-address').tooltip()

   $(document).ready(function(){
 
     $('#contact-form').validate(
     {
      rules: {
        companyname: {
          minlength: 2,
          required: false
        },

        address: {
          required: false,
          minlength: 2
        },

        contactperson: {
          required: false,
          minlength: 2
        },
        contact: {
          minlength: 2,
          required: false
        },
        faxno: {
          minlength: 2,
          required: false
        }
      },
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
      },
      success: function(element) {
        element
        .closest('.form-group').removeClass('has-error').addClass('has-success');
      }
    });
}); // end document.ready

   $(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
</script>
</body>
</html>