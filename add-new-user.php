<?php include 'connect.php'; ?>
<!doctype html>
<html lang="en">
<head>
  <title>Add New User</title>
  <?php include 'header/header-inc.php';?>
  <script type="text/javascript">
  function enableButton() {
    if(document.getElementById('option').checked){
      document.getElementById('b1').disabled='';
    } else {
      document.getElementById('b1').disabled='true';
    }
  }
  </script>
</head>
<body>
  
    <div id="wrapper">

      <?php include 'header/header-admin.php'; ?>

      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            <h1>Add New User</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="icon-file-alt"></i> Add New User</li>
            </ol>
          </div>
          <div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
              <div class="panel-heading"><i class="fa fa-plus-square"></i> Add New user</div>
              <div class="panel-body">
                <form enctype="multipart/form-data"  id="contact-form" class="form-horizontal" role="form" method="post" action="add-new-user.php">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-user"></span>
                        </span>
                        <input type="text" name="name" class="form-control" id="inputName" placeholder="Full Name">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12" data-toggle="tooltip" data-placement="top" title="Address" id="user-address">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-home"></span>
                        </span>
                        <textarea size="3" name="address" class="form-control" id="inputAddress">
                        </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-earphone"></span>
                        </span>
                        <input type="text" name="contact" class="form-control" id="inputContact" placeholder="Contact Number">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-envelope"></span>
                        </span>
                        <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-envelope"></span>
                        </span>
                        <input type="text" name="title" class="form-control" id="inputName" placeholder="Position/Designation">
                      </div>
                    </div>
                  </div>
                   <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-envelope"></span>
                        </span>
                        <input type="file" name="file" class="form-control" id="inputName" >
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">  
                          <span class="glyphicon glyphicon-lock"></span>
                        </span>
                        <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-repeat"></span>  
                        </span>
                        <input type="password" name="cpass" class="form-control" id="inputConfirm" placeholder="Confirm Password">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10" data-toggle="tooltip" data-placement="top" title="Agree to the Terms and Condition to continue" id="terms-condition">
                     <input type="checkbox" type="checkbox"name="option" id="option" onclick="javascript:enableButton();">
                     I agree to the <a href="">Terms and Conditions</a>
                   </div>
                 </div>
                 <button type="submit" class="btn btn-primary btn-block"  name = "b1" id="b1"  onclick="form.action='registration.ph'method='post' ;" disabled>Submit</button>
               </form>
             </div><!--/.panel-body -->
           </div><!--/.panel-primary -->
           <!--start -->
          
           <!--php connection -->
           <?php
           if(isset($_POST['name'])){

             $name = ($_POST['name']);
             $city = ($_POST['address']);
             $email = ($_POST['email']);
              $title = ($_POST['title']);
             $password = ($_POST['password']);
             $confirm = ($_POST['cpass']);
             $contact = ($_POST['contact']);

             $time = time();
             $actual_time = date ('M d Y @H:i:s', $time );

             $errors = array();

             if(!$name){
        
              $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Name is not defined!</div>";
            }


            if(!$city){
              $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Address is not defined!</div>";
            }

            if(!$password){
              $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Password is not defined!</div>";
            }

            if($password){
              if(!$confirm){
                $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Confirmation password is not defined!</div>";
              }
            }

            if(!$email){
              $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> E-mail is not defined!</div>";
            }


            if($name){


              $range = range(1,32);
              if(!in_array(strlen($name),$range)){
                $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Name must be between 1 and 32 characters!</div>";
              }
            }


            if($password && $confirm){
              if($password != $confirm){
                $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> Passwords do not match!</div>";
              }
            }

            if($email){
              $checkemail = "/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";
              if(!preg_match($checkemail, $email)){
                $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> E-mail is not valid, must be name@server.tld!</div>";
              }
            }



            if($email){
              $sql2 = "SELECT * FROM `qm_user` WHERE `qm_email`='".$email."'";
              $res2 = mysql_query($sql2) or die(mysql_error());

              if(mysql_num_rows($res2) > 0){
                $errors[] = "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'><strong>&times;</strong></button> <i class='fa fa-warning'></i> The E-mail address you supplied is already in use of another user!</div>";
              }
            }

            if(count($errors) > 0){
              echo "
              </div><!-- /panel closing -->
              <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              ";
              foreach($errors AS $error){
                echo "$error";
              }
              echo "</div><!-- /error closing -->";
            }else {
              echo "
              </div>
              <div class='col-lg-6 col-md-6 col-xs-12 col-sm-12'>";
      //for the timecode
  //end
  //end
  
  
    $uploaddir = '/home/dmdevtea/public_html/qmaker/tcpdf/examples/images/'; 
    $uploadfile = $uploaddir ."$name-". basename($_FILES['file']['name']);
  $uploadlink = "$name-".basename($_FILES['file']['name']);
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
  
              $time = time();
              $actual_time = date ('M d Y @H:i:s', $time );

              $sql4 = "INSERT INTO `qm_user`
              (`qm_name`,`qm_contact`,`qm_address`,`qm_email`,`qm_password`,`qm_dateregister`,`qm_title`,`qm_directory`)

              VALUES ('".$name."','".$contact."','".$city."','".($email)."','".($password)."',
                '".$actual_time."','".($title)."','".($uploadlink)."')";

$res4 = mysql_query($sql4) or die(mysql_error());

echo "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <i class='fa fa-check-circle'></i> You have successfully registered with the email <b>'$email'</b> and the password of <b>'$password'</b></span>!has</div><!--/close notif -->";

}
else
{
echo "error";
}

}
}

else
{

}

?>
<!--ending -->
<!-- //</div> -->
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script>
  $(".alert").alert()
    $('#terms-condition').tooltip()
    $('#user-address').tooltip()

    $(document).ready(function(){
 
     $('#contact-form').validate(
     {
      rules: {
        name: {
          minlength: 2,
          required: false
        },

        address: {
          required: false,
          minlength: 2
        },

        contact: {
          required: false,
          minlength: 2
        },
        email: {
          required: false,
          email: true
        },
        password: {
          minlength: 2,
          required: false
        },
        cpass: {
          minlength: 2,
          required: false
        }
      },
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
      },
      success: function(element) {
        element
        .closest('.form-group').removeClass('has-error').addClass('has-success');
      }
    });
}); // end document.ready


    $(".alert").alert();
        window.setTimeout(function() 
          { 
            $(".alert").alert('close'); 
          }, 5000);
  </script>
  
</body>
</html>